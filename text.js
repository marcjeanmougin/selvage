"use strict";
/** * WELCOME TO HELL ** */

/*
 * The mechanism for text is the following : we try to keep in sync the textarea
 * where the user can edit texts (on the right of the screen), and the svg
 * itself. Ideally, we should support bold, italic, and many things, but in
 * practive... well... that's not easy.
 * 
 * txt2svg is called to update the svg counterpart of the text (on-the-fly when
 * typing)
 * 
 * svg2txt is called once per edit at the start of it, and should reconstruct in
 * the textarea the text as it is present in the image.
 * 
 * make sure that txt2svg(svg2txt(svg))==svg if possible. (we don't care about
 * the reverse)
 * 
 */

function Txt(){
  if(ModeText.txt) ModeText.txt.remove();
  $("ul.tabs").data("tabs").click(1);// see corresponding html
  var a=$('#tab_text');
  
  var content="<button onclick=Txt.setStyle('font-weight','bold')><b>B</b></button>&nbsp;"
      +"<button onclick=Txt.setStyle('font-style','italic')><i>I</i></button>&nbsp;"
      +"<button onclick=\"Txt.setStyle('text-align','start');Txt.setStyle('text-anchor','start');\">l&nbsp;</button>&nbsp;"
      +"<button onclick=\"Txt.setStyle('text-align','center');Txt.setStyle('text-anchor','middle');\">c</button>&nbsp;"
      +"<button onclick=\"Txt.setStyle('text-align','end');Txt.setStyle('text-anchor','end');\">&nbsp;r</button>&nbsp;"
      
      +"<select onchange=\"Txt.setStyle('font-family','\\\''+this.value+'\\\'')\">";
  
  var fonts=["Sans", "Comic Sans MS", "Monospace", "Times New Roman"];
  for( var ii=0; ii<fonts.length; ii++){
    var i=fonts[ii];
    content+="<option value=\""+i+"\">"+i+"</option>";
  }
  content+="</select>"

  +"<select onchange=\"Txt.setStyle('font-size',this.value+'px')\">";
  var sizes=[4, 6, 8, 9, 10, 11, 12, 13, 14, 16, 18, 20, 22, 24, 28, 32, 36,
      40, 48, 56, 64, 72, 144];
  for( var ii=0; ii<sizes.length; ii++){
    var i=sizes[ii];
    content+="<option value=\""+i+"\" "+(i==16 ? "selected=\"selected\"" : "")
        +">"+i+"</option>";
  }
  content+="</select>"
      
      +'<div contenteditable="true" id="my_txt" style="height:50vh;margin-right:10px;resize:none;background-color:#eeeeff;border:1px solid black;margin-left:10px;overflow:auto;"'
      +'oninput="Txt.txt2svg()">'
      +this.svg2txt(get(ModeText.txtid).node.outerHTML)+'</div>';
  
  a.html(content);
  $("#my_txt").focus();
};

var flatten=function(elt,style){
  var st=style;
  var cur=elt.firstChild;
  var ans="";
  while(cur!=null){
    if(cur.nodeType==3){// textNode
      var t=document.createElement("span");
      t.setAttribute("style", st);
      t.appendChild(cur.cloneNode(false));
      ans+=t.outerHTML;
    }else if(cur.nodeName=="BR"){
      ans+="<br/ >";
    }else{
      ans+=flatten(cur, st+(cur.getAttribute("style")||""));
    }
    cur=cur.nextSibling;
  }// end for
  return ans;
};

var optimize=function(stuff){
  console.log("optimize", stuff);
  stuff=stuff.replace2('style=\"\"', "");// optim
  stuff=stuff.replace(/<tspan style=[^>]*><\/tspan>/, "", "g");// optim
  stuff=stuff.replace(/[a-z\-]*: [a-z0-9]*;/);
  stuff=stuff
      .replace(
          /<tspan style=([^>]*)>([^<]*)<\/tspan><tspan style=([^>]*)>([^<]*)<\/tspan>/,
          function(match,p1,p2,p3,p4){
            console.log(p1, p2, p3, p4);
            if(p1!=p3) return match;
            else return "<tspan style="+p1+">"+p2+p4+"</tspan>";
          }, "g");// optim not working (?)
  return stuff;
};

Txt.txt2svg=function(){
  var txtid=get(ModeText.txtid);
  var stuff=$("#my_txt")[0].innerHTML;
  // var stuff=txt2;
  stuff=stuff.replace2("<br>", "<br/ >");
  stuff=stuff.replace2("<div>", "<br/ >");// fu chrome
  stuff=stuff.replace2("</div>", "");// fu chrome
  stuff="<div>"+stuff+"</div>";
  stuff=stuff.replace2("<br/ ><br/ >", "<br/ >&nbsp;<br/ >");
  stuff=$(stuff)[0];
  stuff=flatten(stuff, "");
  // var ans=stuff;
  console.log("rdyhtml", stuff);
  
  var txt=$(stuff);
  // FIREFOX
  txtid.clear();
  var line=txtid.create(new TSpan());
  for( var i=0; i<txt.length; i++){
    if(txt[i].nodeName=="br"||txt[i].nodeName=="BR"){
      line=txtid.create(new TSpan());
      line.set("dy", 19);
      line.set("x", 0);
    }else{
      var x=line.create(new TSpan());
      x.set("style", txt[i].getAttribute("style"));
      x.create(new TextNode(txt[i].textContent));
    }
  }
  ui.unselectObj(ModeText.txtid);
  ui.selectObj(ModeText.txtid);
  return 0;
};

Txt.setStyle=function(what,how){
  var w=window.getSelection().getRangeAt(0);
  var n=w.startContainer.nodeType==3 ? w.startContainer.parentNode
      : w.startContainer;
  if(n.nodeName=="DIV"&&n.firstChild.nodeType!=3) n=n.firstChild;
  // console.log($(n)[0].outerHTML,$(n).css("font-weight"),$(n).css("font-style"));
  if(what=="font-weight"
      &&($(n).css("font-weight")==700||$(n).css("font-weight")=="bold")) how="normal";
  if(what=="font-style"&&$(n).css("font-style")=="italic") how="normal";
  var t=document.createElement("span");
  $(t).css(what, how);
  t.appendChild(w.extractContents());
  function parcours_recursif(elt,what){
    var cur=elt.firstChild;
    while(cur!=null){
      if(cur.nodeType==3){// textNode
      }else if(cur.nodeName=="BR"){
      }else{
        $(cur).css(what, "");
        parcours_recursif(cur, what);
      }
      cur=cur.nextSibling;
    }// end for
  }
  parcours_recursif(t, what);
  w.insertNode(t);
  // select inside
  var sel=window.getSelection();
  var range=document.createRange();
  range.selectNodeContents(t);
  sel.removeAllRanges();
  sel.addRange(range);
  this.txt2svg();
};

// WARNING : this state of things CANNOT be considered as functional (but as
// better than nothing)
Txt.prototype.svg2txt=function(svg){
  if(!svg) return ""; // WARNING : TODO : chromium is always doing that
  var a=$(svg)[0].childNodes;
  var x="";
  for( var i=0; i<a.length; i++){
    x+=a[i].innerHTML+'<br/>';
  }
  console.log(x);
  return x;
};

Txt.prototype.remove=function(){
  $('#my_txt').remove();
};
