"use strict";
/*
 * Zoom and panning functions.
 * 
 */

// Zoom.js
// June 8, 2013
// track center of screen
var center_real;

function centerScreen(){
  return center_real.s2c();
};

function goDirection(dx,sx,dy,sy){
  if(dx==null) dx=20;
  if(dy==null) dy=20;
  draw.viewbox(vb.x+dx*sx, vb.y+dy*sy, vb.width, vb.height);
  vb=draw.viewbox();
};

function goLeft(d){
  goDirection(d, -1, 0, 0);
};
function goRight(d){
  goDirection(d, 1, 0, 0);
};
function goUp(d){
  goDirection(0, 0, d, -1);
};
function goDown(d){
  goDirection(0, 0, d, 1);
};

function pan_begin(e){
  start_real=new Point(m_.x, m_.y);
  vbb=vb=draw.viewbox();
};

function pan(e){
  var d=m_.minus(start_real);
  draw.viewbox(vbb.x-d.x/vb.zoom, vbb.y-d.y/vb.zoom, vbb.width, vbb.height);
};

function pan_end(e){
  console.log("e");
  vb=draw.viewbox();
};

// zooms
function zoomInOut(p,c){
  if(p==null) p=centerScreen();
  var zz=vb.zoom/c;
  zoomRect({
    x: p.x-c*(p.x-vb.x),
    y: p.y-c*(p.y-vb.y),
    height: window.height/zz,
    width: window.width/zz
  });
};

function zoomIn(p){
  zoomInOut(p, 1./Math.sqrt(2));
};
function zoomOut(p){
  zoomInOut(p, Math.sqrt(2));
};

function zoomAspects(x){
  var c=centerScreen();
  zoomRect({
    x: c.x-width*x,
    y: c.y-height*x,
    height: window.height*x*2,
    width: window.width*x*2
  });
};

function zoom1(){
  zoomAspects(.5);
};
function zoom12(){
  zoomAspects(1);
};
function zoom21(){
  zoomAspects(.25);
};

function zoomDraw(){
  var r=work.box();
  zoomRect(r);
};

function zoomSel(){
  if(!selection.selection.length) return;
  var r=selection.selBox;// console.log(r);
  zoomRect(r);
};

function zoomRect(r){
  if(r.width==0) return;
  if(r.height/r.width>=height/width){
    var ww=width/height*r.height;
    r.x=r.x-(ww-r.width)/2;
    r.width=ww;
  }else{
    var ww=height/width*r.width;
    r.y=r.y-(ww-r.height)/2;
    r.height=ww;
  }
  draw.viewbox(r);
  ui.update_with_zoom();
  vb=draw.viewbox();
};
