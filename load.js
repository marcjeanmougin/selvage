"use strict";
/*
 * This code should take an *arbitrary* svg to upload, process it, and include
 * it in the canvas. Main reason why i should support all svg, and for now the
 * only way to have non-selvage-generated code.
 * 
 * This code is ALLOWED to change incoming code to replace some with equivalent
 * selvage-supported svg code (and will do it, but as for now does not do it)
 * 
 */

var Load={};

Load.process=function(data){
  var uid=++__id__;
  if(!data||data.activeElement.nodeName=="parsererror"){
    console.log(data);
    return;
  }
  var d=data.activeElement.childNodes;
  // console.log(d=data.activeElement.childNodes);
  /* cool, it's already parsed. Thx jQuery. */
  var w=work.create(new Group());
  w.sel();
  for( var i=0; i<d.length; i++){
    if((d[i].data)&&((/^\s*$/.test(d[i].data)))) continue;
    // console.log(d[i]);
    Load.insert(w, d[i], uid);
  }
};

Load.load=function(url){
  $.get(url, function(data){
    Load.process(data);
  });
};

Load.insert=function(toWhere,what,uid){
  // console.log("inserting", toWhere, what);
  if(what.prefix&&what.prefix!="svg") return;// do NOT take sodipodi:thingy nor
  // inkscape:thing (or other)
  var tw=toWhere;
  // insert element (based on tagName)
  
  var obj=what.tagName;
  var objArgs=[null, null, null, null, null];
  
  if(obj==undefined){
    console.log("whereami");
    if(what.data){
      obj="textNode";
      objArgs=[what.textContent];
    }
  }
  
  var n=createObject(obj, objArgs);
  if(n==undefined) return; // do not process unknown objects
  tw.create(n);
  
  if(what.tagName=="image"){
    var where=null;
    for(i in what.attributes){
      if(what.attributes[i].name=="xlink:href"){
        where=what.attributes[i].value;
      }
    }
    if(where) n.setWhere(where);
  }
  
  if(what.tagName=="path"){
    for(i in what.attributes){
      if(what.attributes[i].name=="d"){
        var d=what.attributes[i].value.replace(/^m/, "M");
        n.setD(d);
      }
    }
  }
  
  // recursively insert its children
  var d=what.childNodes;
  for( var i=0; i<d.length; i++){
    if((d[i].data)&&/^\s*$/.test(d[i].data)) continue;
    // console.log(d[i]);
    Load.insert(n, d[i], uid);
  }
  
  if(what.tagName=="style"){// postprocess ids
    console.log(what, n);
    // if(xx=what.attributes[i].value.match(/^url\(\"?#(.*)\"?\)/)){
    // what.attributes[i].value="url(#ijs"+uid+xx[1]+")";
  }
  
  // insert its attributes
  for(i in what.attributes){
    if(!what.attributes[i].value) continue;
    if(what.attributes[i].name=="style") Load.style(what.attributes[i].value,
        n, what.style, uid);
    else if(what.attributes[i].name=="transform"){
      n.applyMatrix(Load.transform(what.attributes[i].value));
    }
    /*
     * we should process "styling" elements after "style" attribute, and
     * .style() them.
     * 
     */
    else{
      var xx;
      if(what.attributes[i].name=="id"){
        what.attributes[i].value="ijs"+uid+what.attributes[i].value;
      }
      if(xx=what.attributes[i].value.match(/^url\(\"?#(.*)\"?\)/)){
        what.attributes[i].value="url(#ijs"+uid+xx[1]+")";
      }
      if((xx=what.attributes[i].value.match(/^#(.*)$/))
          &&what.attributes[i].name.match(/href/)){
        what.attributes[i].value="#ijs"+uid+xx[1]+"";
      }
      n.set(what.attributes[i].name, what.attributes[i].value);
    }
  }
  if(n.applyMatrix) n.applyMatrix(new Matrix());
};

// transform a transform as per
// http://www.w3.org/TR/SVG/coords.html#TransformAttribute
// to matrix
var regmatrix=/matrix\(([e\-0-9\.]+)[, ]+([e\-0-9\.]+)[, ]+([e\-0-9\.]+)[, ]+([e\-0-9\.]+)[, ]+([e\-0-9\.]+)[, ]+([e\-0-9\.]+)\)$/;
var regtranslate=/translate\(([e\-0-9\.]+)([, ]+([e\-0-9\.]+))?\)$/;
var regscale=/scale\(([e\-0-9\.]+)([, ]+([e\-0-9\.]+))?\)$/;
var regrotate=/rotate\(([e\-0-9\.]+)([, ]+([e\-0-9\.]+)[, ]+([e\-0-9\.]+))?\)$/;
var regskewX=/skewX\(([e\-0-9\.]+)\)$/;
var regskewY=/skewY\(([e\-0-9\.]+)\)$/;

Load.transform=function(data){
  return Load.transform2(data);// TODO debug
  var m=new Matrix();
  var x;
  do{
    x=Load.transform2(data);
    if(!x) break;
    m=x[0].multiply(m);
    data=data.slice(x[1].length);
  }while(data!="")
  return m;
};

Load.transform2=function(data){
  var ans;
  var x="";
  // console.log(data);
  if(x=data.match(regmatrix)){
    // console.log(x);
    ans=new Matrix(parseFloat(x[1]), parseFloat(x[2]), parseFloat(x[3]),
        parseFloat(x[4]), parseFloat(x[5]), parseFloat(x[6]));
  }else if(x=data.match(regtranslate)){
    ans=new Matrix(1, 0, 0, 1, parseFloat(x[1]), parseFloat(x[3])||0);
  }else if(x=data.match(regscale)){
    ans=new Matrix(parseFloat(x[1]), 0, 0, parseFloat(x[3])||parseFloat(x[1]),
        0, 0);
  }else if(x=data.match(regrotate)){// TODO : center (maybe preprocess?)
    ans=new Matrix(Math.cos(parseFloat(x[1])), Math.sin(parseFloat(x[1])),
        -Math.sin(parseFloat(x[1])), Math.cos(parseFloat(x[1])), 0, 0);
  }else if(x=data.match(regskewX)){
    ans=new Matrix(1, 0, Math.tan(parseFloat(x[1])), 1, 0, 0);
  }else if(x=data.match(regskewY)){
    ans=new Matrix(1, Math.tan(parseFloat(x[1])), 0, 1, 0, 0);
  }else return new Matrix(1, 0, 0, 1, 0, 0);
  return ans;
};

/* Load.style(what.attributes[i].value,n, what.style); */

Load.style=function(data,object,style,uid){
  // console.log("load style failed", data, object, style);
  // console.log("style", style);
  /* _temp_ object.set("style", data);// TODO */
  for( var prop=0; prop<style.length; prop++){
    var b=style[dashToCamel(style[prop])];
    if(style[dashToCamel(style[prop])]!=undefined){
      var xx;
      if(xx=b.match(/^url\(\"?#(.*)\"?\)/)){
        b="url(#ijs"+uid+xx[1]+")";
      }
      object.style(dashToCamel(style[prop]), b);
    }else console.log(style, style[prop], style[style[prop]], "???");
  }
};
