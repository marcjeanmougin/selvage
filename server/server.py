#!/usr/bin/python3 -u

from drawing import Drawing

import json
import re
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.websocket
import datetime

drawings = {}

def vacuum(drawing):
  try:
    f = open(drawing.filename() + '.dump', 'r')
    for line in f:
      drawing.process(json.loads(line))
    f.close()
  except IOError:
    pass
  f = open(drawing.filename() + '.dump', 'w')
  for line in drawing.dumpCommands():
    f.write(json.dumps(line) + "\n")
  f.close()
  f = open(drawing.filename() + '.svg', 'w')
  f.write(drawing.dumpXML())
  f.close()

class WSHandler(tornado.websocket.WebSocketHandler):
  drawing = None
  def on_message(self, rawMessage):
    time=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    global drawings
    if(self.drawing):
      print(str(time)+ ' '+ self.request.remote_ip  +' \033[93m'+self.drawing.name+'\033[0m '+ rawMessage)
    else:
      print(str(time)+' '+ self.request.remote_ip +' \033[93m'+rawMessage+'\033[0m')
    message=json.loads(rawMessage)
    if message['type'] == 'open':
      drawing = message['drawing']
      if drawing not in drawings.keys():
        drawings[drawing] = Drawing(drawing)
        vacuum(drawings[drawing])
        drawings[drawing].openFile()
      self.drawing = drawings[drawing]
      self.drawing.addSubscriber(self)
      print(str(time)+' '+ self.request.remote_ip +' \033[93m'+self.drawing.name+'\033[0m'+' \033[92m sending drawing init message to client\033[0m')
      for command in self.drawing.dumpCommands():
        print(str(time)+' '+ self.request.remote_ip +' \033[93m'+self.drawing.name+'\033[0m'+' \033[92m'+json.dumps(command)+'\033[0m')
        self.write_message(json.dumps(command))
    elif message['type'] == 'save':
      f = open(self.drawing.filename() + '.svg', 'w')
      f.write(self.drawing.dumpXML())
      f.close()
    else:
      self.drawing.writeCmd(rawMessage)
      self.drawing.process(message)
      for c in self.drawing.subscribers:
        if c != self:
          print(str(time)+' '+ self.request.remote_ip +' \033[94m>sending message to client\033[0m')
          c.write_message(rawMessage)

  def on_close(self):
    time=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print(str(time)+' '+ self.request.remote_ip +' \033[93m'+self.drawing.name+'\033[0m'+' connection closing')
    if self.drawing:
      self.drawing.removeSubscriber(self)
      print(str(time)+' '+ self.request.remote_ip +' \033[93m'+self.drawing.name+'\033[0m'+' unsubscribed')
      # if len(self.drawing.subscribers) == 0:
      #   self.drawing.f.close()
      #   vacuum(self.drawing)
      #   del drawings[self.drawing.name]
      #   print 'drawing flushed'
    print(str(time)+' '+ self.request.remote_ip +' \033[91mconnection closed\033[0m')

application = tornado.web.Application([
    (r'/ws', WSHandler),
])

if __name__ == '__main__':
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(8889)
    tornado.ioloop.IOLoop.instance().start()

