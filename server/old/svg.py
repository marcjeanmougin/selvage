import random
import json

class Svg:
  
  def __init__(self,w):
    self.work=Truc(self)
    self.ids={}
    self.ids[w]=self.work
  
  work=0
  ids={} # id-> object

  def process(self,m):
    x=m["data"]
    if(x[0] not in self.ids):
      return
    e=self.ids[x[0]]
    if(m['type']=="mv"):#"mv"://id,what(front, back, forward, backward)
      if(x[1]=="front"):
        e.parent.mvtop(e)
        return;
      if(x[1]=="back"):
        e.parent.mvback(e)
        return;
      if(x[1]=="forward"):
        e.parent.mvup(e)
        return;
      if(x[1]=="backward"):
        e.parent.mvdown(e)
        return;
      return;
    if(m['type']=="sa"):#"sa"://setAttribute[id,attribute,value]
      e.sa(x[1],x[2])
      return;
    if(m['type']=="add"):#"add": //[gid,elt_id,i]
      e2=self.ids[x[1]]
      print e2,e2.parent
      e2.parent.rm(e2)
      e2.parent=e
      xx=-1
      if(len(x)==3):
        xx=x[2]
      e.add(e2,xx)
      return;
    if(m["type"]=="dup"):#"dup" : //duplicate [id, new id,[]]
      e=self.ids[x[0]]
      e.duplicate(x[1])
    if(m['type']=="c"):#"c": //create[where,what,id,args]
      e2=Truc(self)
      e2.parent=e
      e2.type=x[1]
      e2.id=x[2]
      e2.c_args=x[3]
      self.ids[x[2]]=e2
      e.add(e2,-1)
      return;
    if(m['type']=="rm"):#"rm": //delete[id]
      del self.ids[x[0]]
      e.parent.rm(e)      
      return;
    if(m['type']=="txt"):
      self.ids[x[0]].c_args=[x[1]]
      return;
    if(m['type']=="am"):#"am"://applymatrix[id,m]
      e.am(x[1])
      return;
    if(m['type']=="new"):
      a=(  [json.dumps({"type":"load","data":[str(self.work.id)]})]+sum( ((a.toList() ) for a in self.work.content) ,[] ))
      return a
    if(m['type']=="export"):#export[*,width, height]
      a=      '<?xml version="1.0" standalone="no"?>\n<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">\n'
      a+='<svg width="'+str(x[1])+'px" height="'+str(x[2])+'px" xmlns="http://www.w3.org/2000/svg" version="1.1">'
      a+=''.join((a.toXML() ) for a in self.work.content)+'</svg>'
      print a
      return a
   
  

class Truc:
  def __init__(self,s):
    self.svg=s
    self.content=[]
    self.attrs={}
  svg=0
  id=0
  parent=0
  type=""
  c_args=""
  attrs={}
  am_=""
  content=[]
  
  def duplicate(self,nid):
    n =Truc(self.svg)
    n.parent=self.parent
    n.type=self.type
    n.id=nid
    n.c_args=self.c_args[:]
    n.attrs=self.attrs.copy()#does not work ?
    n.am_=self.am_
    self.svg.ids[nid]=n
    print(n,n.parent)
    self.parent.add(n,-1)
    return n
  def add(self, o,n):
    if(o in self.content):
      return
    if(n==-1):
      self.content.append(o)
    else:
      self.content.insert(n,o)
  def sa(self,x,y):
    self.attrs[x]=y
  def am(self,m):
    self.am_=m
  def mvup(self, o):
    x=self.content.index(o)
    if(x<len(self.content)-1):
      self.content[x],self.content[x+1]=self.content[x+1],self.content[x]
  def mvdown(self, o):
    x=self.content.index(o)
    if(x>0):
      self.content[x],self.content[x-1]=self.content[x-1],self.content[x]
  def mvtop(self, o):
    self.content.remove(o)
    self.add(o,-1)
  def mvback(self,o):
    self.content.remove(o)
    self.content.insert(0,o)
  def rm(self, o):
    self.content.remove(o)

  def toXML(self):
    opening=''
    closing=''
    args=(self.c_args)

    if(self.type=="rect"):
      opening='<rect '
      if("width" not in self.attrs):
        opening +=' width="'+str(abs(args[0]['x']-args[1]['x']))+'"'
      if("height" not in self.attrs):
        opening +=' height="'+str(abs(args[0]['y']-args[1]['y']))+'"'
      if("x" not in self.attrs):
        opening +=' x="'+str(min(args[0]['x'],args[1]['x']))+'"'
      if("y" not in self.attrs):
        opening +=' y="'+str(min(args[0]['y'],args[1]['y']))+'" '
      closing='</rect>'
    if(self.type=="star"):#p,c,prefs[n_pics,star_ratio,poly],b
      opening='<path '
      closing='</path>'
    if(self.type=="ellipse"):
      opening='<ellipse '
      rx=abs(args[0]['x']-args[1]['x'])*1./2
      ry=abs(args[0]['y']-args[1]['y'])*1./2
      if("rx" not in self.attrs):
        opening +=' rx="'+str(rx)+'"'
      if("ry" not in self.attrs):
        opening +=' ry="'+str(ry)+'"'
      if("cx" not in self.attrs):
        opening +=' cx="'+str(min(args[0]['x'],args[1]['x'])+rx)+'"'
      if("cy" not in self.attrs):
        opening +=' cy="'+str(min(args[0]['y'],args[1]['y'])+ry)+'" '
      closing='</ellipse>'
    if(self.type=="path"):
      opening='<path '
      if("d" not in self.attrs):
        opening +=' d="'+args[0]+'" '
      closing='</path>'
    if(self.type=="g"):
      opening="<g "
      closing='</g>'
    if(self.type=="text"):
      opening="<text "
      text=(args[0]).split("\n")
      for s in text:
        closing+='<tspan x="0" dy="19">'+text[s]+'</tspan>'
      closing+="</text>"
    if(self.type=="linearGradient"):
      opening="<linearGradient "
      if("x1" not in self.attrs):
        opening +=' x1="'+str(args[0]['x'])+'"'
      if("y1" not in self.attrs):
        opening +=' y1="'+str(args[0]['y'])+'"'
      if("x2" not in self.attrs):
        opening +=' x2="'+str(args[1]['x'])+'"'
      if("y2" not in self.attrs):
        opening +=' y2="'+str(args[1]['y'])+'" '
      closing="</linearGradient>"
    if(self.type=="radialGradient"):
      opening="<radialGradient "
      if("cx" not in self.attrs):
        opening +=' cx="'+str(args[0]['x'])+'"'
      if("cy" not in self.attrs):
        opening +=' cy="'+str(args[0]['y'])+'"'
      if("r" not in self.attrs):
        opening +=' r="'+str(args[1])+'"'
      if("fx" not in self.attrs):
        opening +=' fx="'+str(args[2]['x'])+'"'
      if("fy" not in self.attrs):
        opening +=' fy="'+str(args[2]['y'])+'"'
      closing="</radialGradient>"
    if(self.type=="stop"):
      opening="<stop "
      closing="</stop>"
    if(self.type=="defs"):
      opening='<defs '
      closing='</defs>'
    
    ans=opening+ ' id="'+self.id+'"'
    if(self.am_!=""):
      ans+=' transform=matrix('+m+') '
    for k in self.attrs:
      ans+=' '+k+'="'+self.attrs[k]+'" '
    ans+='>'
    for a in self.content:
      ans+=a.toXML()
    ans+=closing
    return ans

  def toList(self):#[where,what,id,args]
    ans= ( [json.dumps({"type":"c","data":[self.parent.id,self.type,self.id,self.c_args]})]+sum( (a.toList() for a in self.content) ,[])+[json.dumps({"type":"sa","data":[self.id,k,self.attrs[k]]}) for k in self.attrs])
    if(self.am_!=""):
      ans=ans+[ json.dumps({"type":"am","data":[self.id,self.am_]})];
    return ans
    
      

