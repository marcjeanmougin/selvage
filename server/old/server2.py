#!/usr/bin/python

#protocol : 
#"load" [work]
from svg import *
import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web

import random
import json

#we have a map {id_svg -> [clients]}
clients={}
#and a map {id_svg -> objet_svg }
objects={}




class WSHandler(tornado.websocket.WebSocketHandler):
  id=-1
  r=0
  clipboard=0 #unused (yet)

  def open(self):
    print 'new connection'
    self.r=random.random()

  def on_message(self, message):
      global clients,objects
      print 'message received %s' % message
      m=json.loads(message)
      if m['type']=="new":
          self.new=1
          self.id=m['data'][0]
          if(self.id in objects):
              clients[self.id].append(self)
              for x in objects[self.id].process(m):
                self.write_message(x)
          else:
              self.write_message(json.dumps({"type":"new","data":[str(self.id)]}))
              clients[self.id]=[self]
          return
      if m['type']=="c" and self.new==1:
          #sends work
        self.new=0
        if(self.id not in objects):
          objects[self.id]=Svg(self.id)
          objects[self.id].work.id=m["data"][2]
          objects[self.id].ids[m["data"][2]]=objects[self.id].work
        return
      if m['type']=="export":
        m2=objects[self.id].process(m)
        self.write_message(json.dumps({"type":"export","data":m2}))
        return;
  #else, broadcast
      objects[self.id].process(m)
      for c in clients[self.id]:
          if c.r!= self.r:
              c.write_message(message)
          else:
            print("sending"+message)


 
  def on_close(self):
      if self.id > -1 :
          clients[self.id].remove(self)
      print 'connection closed'
 
 
application = tornado.web.Application([
    (r'/ws', WSHandler),
])
 
 
if __name__ == "__main__":
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(8889)
    tornado.ioloop.IOLoop.instance().start()
