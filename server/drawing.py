from base64 import b32encode
import xml.etree.ElementTree
from xml.etree.ElementTree import SubElement, tostring

class Drawing:
  def __init__(self, name):
    self.name = name
    self.subscribers = set()
    # must be a valid xml id and be predictable by client
    self.root = Element('g', 'root', [])
    self.elements = {}
    self.registerElement(self.root)
    self.f = None

  def openFile(self):
    self.f = open(self.filename() + '.dump', 'a')

  def filename(self):
    return 'drawings/' + str(b32encode(self.name.encode('utf-8')))

  def addSubscriber(self, subscriber):
    self.subscribers.add(subscriber)
  
  def removeSubscriber(self, subscriber):
    self.subscribers.remove(subscriber)

  def registerElement(self, element, parent=None, position=None):
    self.elements[element.id] = element
    if parent:
      parent.addChild(element, position)

  def dumpCommands(self):
    # do not instruct the client to create the root element
    return self.root.dumpChildren()

  def dumpXML(self):
    root = xml.etree.ElementTree.Element('svg')
    self.root.dumpXML(root)
    prolog = """<?xml version="1.0" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
"""
    root.set('xmlns', "http://www.w3.org/2000/svg")
    root.set('xmlns:xlink', "http://www.w3.org/1999/xlink")
    root.set('version', "1.1")
    return str(prolog) + (tostring(root)).decode('utf-8')

  def writeCmd(self, rawCmd):
    self.f.write(rawCmd + '\n')
    self.f.flush()

  def process(self, cmd):
    if cmd['type'] == 'new':
      parent = self.elements[cmd['objParent']]
      args = cmd.get('objArgs', [])
      newElement = Element(cmd['objType'], cmd['objId'], args, parent)
      self.registerElement(newElement, parent, cmd.get('objPosition', -1))
    elif cmd['type'] == 'setAttribute':
      if 'value' in cmd:
        self.elements[cmd['id']].attributes[cmd['attribute']] = cmd['value']
      if cmd['attribute']=='id':
        self.elements[cmd['value']]=self.elements.pop(cmd['id'])
    elif cmd['type'] == 'applyMatrix':
      self.elements[cmd['id']].matrix = cmd['matrix']
    elif cmd['type'] == 'move':
      element = self.elements[cmd['id']]
      newParent = self.elements[cmd['objParent']]
      #print 'DEBUG: children was'
      #for child in newParent.children:
      #  print child.id
      element.parent.removeChild(element)
      element.parent = newParent
      #print "parent of %s is %s" % (element.id, element.parent.id)
      newParent.addChild(element, cmd.get('objPosition', -1))
      #print 'DEBUG: children now'
      #for child in newParent.children:
      #  print child.id
    elif cmd['type'] == 'copy':
      element = self.elements[cmd['id']]
      copy = element.getCopy(cmd['objId'])
      self.registerElement(copy, element.parent, cmd.get('objPosition', -1))
    elif cmd['type'] == 'edit':
      self.elements[cmd['id']].args[0] = cmd['value']
    elif cmd['type'] == 'delete':
      element = self.elements[cmd['id']]
      element.parent.removeChild(element)
      del self.elements[cmd['id']]

class Element:
  @property
  def id(self):
    return self.attributes['id']

  def __init__(self, name, id, args, parent=None):
    self.name = name
    self.children = []
    self.attributes = {'id': id}
    self.matrix = None
    self.parent = parent
    self.args = args

  def dumpCommands(self):
    return self.dump() + self.dumpChildren()

  def dumpChildren(self):
    return sum([c.dumpCommands() for c in self.children], [])

  def addChild(self, child, position):
    if position == -1:
      position = len(self.children)
    self.children.insert(position, child)

  def removeChild(self, child):
    self.children.remove(child)

  def getCopy(self, id):
    copy = Element(self.name, self.id, list(self.args), self.parent)
    copy.children = []
    copy.attributes = dict(self.attributes)
    copy.attributes['id'] = id
    copy.matrix = self.matrix
    return copy

  def setRect(self, node, args1, args2):
    """reconstruct x, y, width and height from objArgs"""
    if isinstance(args1, dict) and isinstance(args2, dict):
      if("width" not in self.attributes.keys()):
        node.set('width', str(abs(args1['x']-args2['x'])))
      if("height" not in self.attributes.keys()):
        node.set('height', str(abs(args1['y']-args2['y'])))
      if("x" not in self.attributes.keys()):
        node.set('x', str(min(args1['x'],args2['x'])))
      if("y" not in self.attributes.keys()):
        node.set('y', str(min(args1['y'],args2['y'])))

  def dumpXML(self, parent):
    node = SubElement(parent, self.name if self.name != 'star' else 'path')
    args = self.args
    for attr in self.attributes.keys():
      val = self.attributes[attr]
      # ElementTree cannot serialize ints and floats directly...
      node.set(attr, val if isinstance(val, str) else str(val))
    if self.matrix:
      node.set('transform', "matrix(%s, %s, %s, %s, %s, %s)" % 
          (self.matrix['a'], self.matrix['b'], self.matrix['c'],
           self.matrix['d'], self.matrix['e'], self.matrix['f']))
    if(self.name=="rect"):
      self.setRect(node, args[0], args[1])
    if(self.name=="star"):
      node.set('d', self.args[3])
    if(self.name=="image"):
      node.set('xlink:href', self.args[0])
    if(self.name=="ellipse" and isinstance(args[0], dict) and
        isinstance(args[1], dict)):
      rx=abs(args[0]['x']-args[1]['x'])*1./2
      ry=abs(args[0]['y']-args[1]['y'])*1./2
      if("rx" not in self.attributes.keys()):
        node.set('rx', str(rx))
      if("ry" not in self.attributes.keys()):
        node.set('ry', str(ry))
      if("cx" not in self.attributes.keys()):
        node.set('cx', str(min(args[0]['x'],args[1]['x'])+rx))
      if("cy" not in self.attributes.keys()):
        node.set('cy', str(min(args[0]['y'],args[1]['y'])+ry))
    if(self.name=="path"):
      if("d" not in self.attributes):
        node.set('d', args[0])
    #if(self.name=="textNode"):
     # text=(args[0])
      #.split("\n")
      #for s in text:
       # tspan = SubElement(node, "tspan")
       # tspan.set('x', '0')
       # tspan.set('dy', '19')
       # tspan.text = s
    if(self.name=="linearGradient"):
      if("x1" not in self.attributes and isinstance(args[0], dict)):
        node.set('x1', str(args[0]['x']))
      if("y1" not in self.attributes and isinstance(args[0], dict)):
        node.set('y1', str(args[0]['y']))
      if("x2" not in self.attributes and isinstance(args[1], dict)):
        node.set('x2', str(args[1]['x']))
      if("y2" not in self.attributes and isinstance(args[1], dict)):
        node.set('y2', str(args[1]['y']))
    if(self.name=="radialGradient"):
      if("cx" not in self.attributes and isinstance(args[0], dict)):
        node.set('cx', str(args[0]['x']))
      if("cy" not in self.attributes and isinstance(args[0], dict)):
        node.set('cy', str(args[0]['y']))
      if("r" not in self.attributes):
        node.set('r', str(args[1]))
      if("fx" not in self.attributes and isinstance(args[2], dict)):
        node.set('fx', str(args[2]['x']))
      if("fy" not in self.attributes and isinstance(args[2], dict)):
        node.set('fy', str(args[2]['y']))
    
    for child in self.children:
      child.dumpXML(node)

  def dump(self):
    d = {'type': 'new', 'objType': self.name, 'objId': self.id,
          'objArgs': self.args}
    if self.parent:
      d['objParent'] = self.parent.id
    return ([d] +
        [{'type': 'setAttribute', 'attribute': a, 'value': self.attributes[a],
          'id': self.id} for a in self.attributes.keys() if a != 'id'] +
        ([{'type': 'applyMatrix', 'matrix': self.matrix, 'id':self.id}]
            if self.matrix else []))

