"use strict";
/*
 * Path modification handles
 */

//
function Handle(pathid,n,n2){
  this.pathid=pathid;
  this.n=n;
  this.n2=n2;
  var p=get(pathid).getPointCoord(n, n2);
  var delta=4/vb.zoom;
  var d=new Point(delta, delta);
  Rect.call(this, p.minus(d), p.add(d));
  this.node.setAttribute("fill", "#66e");
  this.node.setAttribute("id", "handle_"+pathid+"_"+n+"_"+n2);
  
  this.on("mouseover", function(e){
    e.stopPropagation();
    window.handle=this.instance;
  });
  // this.on("mouseleave", function(){
  // window.handle=null;
  // });
}

Handle.prototype=new Rect();
Handle.prototype.contructor=Handle;

Handle.prototype.drag=function(d){
  this.translate(d, true);
  get(this.pathid).movePoint(this.n, d, this.n2);
};

function Handles(p){
  // p being a path
  if(mode!=ModePathEdit) return;
  
  if(ui.han){
    Handles.remove();
  }
  console.log(selection.selection);
  if(selection.selection.length!=1||!selection.selection[0].match(/path/)) return;
  ui.han=[];
  get(selection.selection[0]).segments.forEach(function(x,i){
    if(x.type=="z"||x.type=="Z") return;
    if(x.type=="c"||x.type=="C"){
      
      ui.han.push(ui_artifacts.create(
          new Path("M"+get(selection.selection[0]).points[i-1]+"L"
              +get(selection.selection[0]).getPointCoord(i, 0)), true).set(
          "stroke", "black", true).set("stroke-width", 1/vb.zoom+"", true));
      ui.han.push(ui_artifacts.create(
          new Path("M"+get(selection.selection[0]).points[i]+"L"
              +get(selection.selection[0]).getPointCoord(i, 1)), true).set(
          "stroke", "black", true).set("stroke-width", 1/vb.zoom+"", true));
    }
    for( var j=0; j<(segmentTypes[x.type]-2)/2; j++){
      ui.han.push(ui_artifacts.uiHandle(selection.selection[0], i, j));
    }
    ui.han.push(ui_artifacts.uiHandle(selection.selection[0], i, j+1));
    
  });
}

Handles.remove=function(){
  if(ui.han) ui.han.forEach(function(x){
    x.remove(true);
  });
  ui.han=null;
};

function handle_drag(){
  var d=m.minus(start_canvas);
  var h=md_handle;
  h.drag(d);
  start_canvas=m;
};

function handle_drag_end(){
  Handles();
};
