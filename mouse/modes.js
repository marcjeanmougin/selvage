"use strict";

/**
 * See mouse.js for comments.
 * 
 * This file is mainly about common parent class for modes
 */

var currently_drawn;

function changeMode(newMode){
  window.mode=Modes[newMode];
  window.mode.init();
  if(newMode) Arrows.remove();
  else Arrows();
  if(newMode!=1) Handles.remove();
  else Handles();
  
};

function setColors(b){
  currently_drawn.set("fill", $("#fillColor").val(), b);
  currently_drawn.set("stroke", $("#strokeColor").val(), b);
  currently_drawn.set("stroke-width", $("#strokeWidth").val(), b);
};

var Mode=function(){
};
Mode.prototype.menu="dummy";
Mode.prototype.cursor="auto";

Mode.prototype.init=function(){
  $("#context").html(this.menu);
  console.log(this.cursor);
  $("#canvas").css('cursor', this.cursor+",default");
};

Mode.prototype.contextual_start_drag=function(){
};
Mode.prototype.contextual_drag=function(){
};
Mode.prototype.contextual_end_drag=function(){
  currently_drawn=cur_action=null;
};
Mode.prototype.contextual_mousemove=function(){
};

Mode.prototype.contextual_click=function(e){
  if(e.shiftKey){
    if(!selectable) return;
    if(selection.has(selectable)) selection.minus(selectable);
    else selection.add(selectable);
  }else if(e.altKey){
    // alternative selection ("select under") TODO
  }else{
    selection.select();
  }
};
Mode.prototype.contextual_dblclick=function(e){
  this.contextual_click(e);
};
