"use strict";
// zoom
var ModeZoom=new Mode();
ModeZoom.cursor="url(img/cursors/cursorZoom.svg) 16 16";
ModeZoom.menu="<a href='#' onclick=zoomIn()><img src='img/zoomIn.svg'></a>"
    +"  <a href='#' onclick=zoomOut()><img src='img/zoomOut.svg'></a>"
    +"  <a href='#' onclick=zoom1()><img src='img/zoom1.svg'></a>"
    +"  <a href='#' onclick=zoom12()><img src='img/zoom12.svg'></a>"
    +"  <a href='#' onclick=zoom21()><img src='img/zoom21.svg'></a>"
    +"  <a href='#' onclick=zoomSel()><img src='img/zoomSel.svg'></a>"
    +"  <a href='#' onclick=zoomDraw()><img src='img/zoomDraw.svg'></a>";
ModeZoom.contextual_start_drag=function(e){
  cur_action="zoom";
};

ModeZoom.contextual_drag=function(e){
  ui.draw_rectangle(m, start_canvas, true);
};

ModeZoom.contextual_end_drag=function(e){
  zoomRect(ui.tempBox.box());
  ui.tempBox.remove(true);
  ui.tempBox=null;
};

ModeZoom.contextual_click=function(e){
  if(!e.shiftKey) zoomIn(m);
  else zoomOut(m);
};
