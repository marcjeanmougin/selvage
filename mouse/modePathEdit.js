"use strict";

// edit node
var ModePathEdit=new Mode();
ModePathEdit.menu="Mode edit node not suported(yet)";
ModePathEdit.cursor="url('img/cursors/cursorPathEdit.svg')";

/*
 * TODO
 * 
 * what really happens in inkscape when drag&drop ? -either we are on a handle,
 * it moves it -either we are not, and selection rectangle -if there are objects
 * selected, this rect selects only handles. -if not, it selects objects.
 * 
 * when we click, -either something is selected, and we click on one of its
 * segments, and it selects the *segment* (ni idea how to do that) -either
 * something is selected and we click on one handle, it is selected, - either it
 * selects objects as normal
 * 
 * Note that we won't support (well, maybe we will, but that's not a priority)
 * inkscape's behavior about modifying segments when clicking on them
 * 
 * This file is mainly calling svg/path.js, and called by mouse.js
 * 
 */

ModePathEdit.contextual_start_drag=function(e){
  if(md_handle){
    cur_action=md_handle;
    Handles.remove();
    return;
  }
};

ModePathEdit.contextual_drag=function(e){
  if(cur_action){
    handle_drag();
    return;
  }
};

ModePathEdit.contextual_end_drag=function(e){
  if(cur_action){
    handle_drag_end();
    return;
  }
  Handles();
};

ModePathEdit.contextual_click=function(e){
  Mode.prototype.contextual_click(e);
  Handles();
};
