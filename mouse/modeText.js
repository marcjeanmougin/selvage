"use strict";
// text
var ModeText=new Mode();

ModeText.prefs={};
ModeText.txtid="";

ModeText.toogleBold=function(){
  if(selection.selection[0]) var a=get(selection.selection[0]);
  else return;
  var b=a._style["font-weight"];
  if(b&&b=="bold") a.style("font-weight", "normal");
  else a.style("font-weight", "bold");
};

ModeText.toogleItalic=function(){
  if(selection.selection[0]) var a=get(selection.selection[0]);
  else return;
  var b=a._style["font-style"];
  if(b&&b=="italic") a.style("font-style", "normal");
  else a.style("font-style", "italic");
};

ModeText.set=function(w,h){
  if(selection.selection[0]) get(selection.selection[0]).style(w, h);
};

ModeText.menu="Text Mode";/*
                           * "<input style='width:150px' type='text'
                           * value=\"Sans\"
                           * onchange='ModeText.set(\"font-family\",this.value+\",Sans\")'/>" +"<input
                           * style='width:50px' type='text' value=\"12\"
                           * onchange='ModeText.set(\"font-size\",this.value+\"px\")'>" +"<a
                           * href='#' onclick=ModeText.toogleBold()>B</a>" +"<a
                           * href='#' onclick=ModeText.toogleItalic()>I</a>" +"<a
                           * href='#'
                           * onclick=\"ModeText.set('text-align','start');ModeText.set('text-anchor','start');\">l</a>" +"<a
                           * href='#'
                           * onclick=\"ModeText.set('text-align','center');ModeText.set('text-anchor','middle');\">c</a>" +"<a
                           * href='#'
                           * onclick=\"ModeText.set('text-align','end');ModeText.set('text-anchor','end');\">r</a>"
                           * +"Text mode (TODO) (soon)";
                           */
ModeText.cursor="url(img/cursors/cursorText.svg) 10 10";

/*
 * disabled due to inkscape bug : flowroot to flow text is NO MORE in the spec
 * 2.0 / 1.2 (and not in the spec 1.1 at all).
 * 
 * Maybe manually implement some feature to newline when needed (later)
 */

/*
 * ModeText.contextual_start_drag=function(e){};
 * ModeText.contextual_drag=function(e){};
 * ModeText.contextual_end_drag=function(e){};
 */

ModeText.contextual_click=function(){
  // try to select a text field. if failure create one.
  if(selectable&&selectable.match(/text/)){
    selection.select();
    if(!selection.selection[0].match(/text/)) return;
  }else{
    cur_action="text";
    currently_drawn=work.create(new Text());
    currently_drawn.translate(m);
    currently_drawn.sel();
    selection.select(currently_drawn.node.id);
  }
  this.txtid=selection.selection[0];
  this.txt=new Txt();
  currently_drawn=null;
  writable=true;
};

ModeText.init=function(){
  $("#context").html(this.menu);
  console.log(this.cursor);
  $("#canvas").css('cursor', this.cursor+",default");
  if(selection&&selection.selection[0]&&selection.selection[0].match(/text/)){
    selection.select(selection.selection[0]);
    this.txtid=selection.selection[0];
    this.txt=new Txt();
    currently_drawn=null;
    writable=true;
  }
};
