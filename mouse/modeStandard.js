"use strict";
// standard
var ModeStandard=new Mode();
ModeStandard.cursor="url()";
ModeStandard.m=1;
ModeStandard.menu="<a href='#' onclick='selection.rotate(new Angle(-90))'><img src='img/rotateC.svg'/></a>"
    +"<a href='#' onclick='selection.rotate(new Angle(90))'><img src='img/rotateCC.svg'/></a>"
    +"<a href='#' onclick='selection.flipH()'><img src='img/flipH.svg'/></a>"
    +"<a href='#' onclick='selection.flipV()'><img src='img/flipV.svg'/></a>"
    +"<a href='#' onclick='selection.backward()'><img src='img/backward.svg'/></a>"
    +"<a href='#' onclick='selection.forward()'><img src='img/forward.svg'/></a>"
    +"<a href='#' onclick='selection.back()'><img src='img/bottom.svg'/></a>"
    +"<a href='#' onclick='selection.front()'><img src='img/top.svg'/></a>"
    +" x=<input id='menu_x' style='width:50px' type='text' name='w' onchange='selection.changeX(this.value);'/>"
    +" y=<input id='menu_y' type='text' name='h' style='width:50px' onchange='selection.changeY(this.value);'/>"
    +" w=<input id='menu_width' type='text' style='width:50px' name='rx' onchange='selection.changeWidth(this.value);'/>"
    +" h=<input id='menu_height' style='width:50px' type='text' name='ry' onchange='selection.changeHeight(this.value)'/>";

ModeStandard.contextual_start_drag=function(e){
  if(md_arrow){
    cur_action="arrow";
    arrow_drag_start();
    Arrows.remove();
    return;
  }// arrows are present only in mode0
  if(!(e.shiftKey)&& // do a selectionBox if shift on already selected
  ((selection.has(md_selectable))// already selected object
  ||selection.select(md_selectable))){// else, deselect, select one object
    cur_action="move";
    Arrows.remove();
  }else{
    // selection box \o/
    cur_action="selectionBox";
  }
};

ModeStandard.contextual_drag=function(e){
  if(cur_action.match(/arrow/)){
    arrow_drag();
    return;
  }
  if(cur_action=="move"){
    selection.translate(m.minus(start_canvas));
    start_canvas=m;
  }else if(cur_action=="selectionBox"){
    ui.draw_rectangle(m, start_canvas, true);
  }
  // depend if selection box opening OR object being dragged.
};

ModeStandard.contextual_end_drag=function(e){
  if(cur_action.match(/arrow/)){
    arrow_drag_end();
    Arrows();
    return;
  }
  if(cur_action=="selectionBox"){
    if(!ui.tempBox) return;
    var b=ui.tempBox.box();
    work.each(function(i,elt){
      if(b.contains(this.box())){
        console.log(this);
        selection.add(this.node.id);
      }
    });
    ui.tempBox.remove(true);
    ui.tempBox=null;
  }
  Arrows();
};

ModeStandard.contextual_click=function(e){
  if(e.shiftKey){
    if(!selectable) return;
    if(selection.has(selectable)) selection.minus(selectable);
    else selection.add(selectable);
  }else if(e.altKey){
    // alternative selection ("select under") TODO
  }else{
    // if click on already selected, toogle arrows
    if(selection.has(selectable)){
      Arrows.toogle();
    }else{ // else select (with arrows)
      selection.select();
    }
  }
  Arrows();
};

ModeStandard.contextual_dblclick=function(){
  // changemode + modificator select according to type TODO
};
