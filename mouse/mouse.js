"use strict";

/*
 * 
 * This file is about all behaviors that have to do with the mouse. We try to
 * mimick Inkscape, so we have to define "modes" in which some basic actions
 * (click, drag, release click, doubleclick) will produce a variety of actions.
 * 
 * All files mode*.js are there to define these actions and are pretty much
 * self-explanatory.
 * 
 * This very file is about the interaction itself : handling of events,
 * conversions between document units and screen units, detecting if the action
 * is more like a click or a clic+drag from the user pov, and so on.
 * 
 * As of 2014, I haven't found a bug in it for a while and consider it *stable*.
 * 
 * Maybe I will have to make some changes to better support the moments when the
 * mouse goes OUT of the canvas, which produces quite a lot of bugs.
 * 
 */

// current position of cursor (real coords)
var m_;

// current position (canvas coords)
var m;

// dragstart position of cursor (real coords)
var start_real;

// dragstart position of cursor (canvas coords)
var start_canvas;

// dragend position of cursor (real coords)
var end_real;

// we are currently dragging ?
var dragging=0;

// allow to know if some key is pressed without having to pass it as argument.
var last_event;

// for doubleclick
var last_mousedown_timestamp=0;

// are we dblclicking ?
var dblclicking=0;

// what were we clicking at start of drag ?
var md_selectable;
var md_arrow;
var md_handle;

var cur_action;

$(function(){
  
  // init
  m_=new Point();
  m=new Point();
  start_real=new Point();
  start_canvas=new Point();
  end_real=new Point();
  
  function mouse_is_down(){
    var e=last_event;
    if(window.dragging&&cur_action!=null){
      mode.contextual_drag(e);
    }else if(m_.distance(start_real)>9){
      window.dragging=1;
      mode.contextual_start_drag(e);
    }
  }
  
  $("#canvas").mousewheel(function(e){
    if(!selection) return;
    // console.log("wheel", e);
    var delta=e.originalEvent.detail;
    if(!delta){
      // webkit support
      delta=-e.originalEvent.wheelDelta/10;
    }
    // console.log("delta", delta);
    e.preventDefault ? e.preventDefault() : e.returnValue=false;
    if(e.ctrlKey){
      if(delta>0){
        zoomOut(m);
      }else{
        zoomIn(m);
      }
    }else if(e.shiftKey){
      goLeft(delta*10/(3*vb.zoom));
    }else{
      goDown(delta*10/(3*vb.zoom));
    }
  });
  
  $("#canvas").mousemove(
      function(e){
        e.preventDefault ? e.preventDefault() : e.returnValue=false;
        if(!selection) return;
        window.m_.x=e.pageX;
        window.m_.y=e.pageY;
        window.m=m_.s2c();
        
        $("#cursor_coords").html(
            "x="+m.x+"y="+m.y+"["+selection.selection.length+"]");
        /*
         * $("#menus").html( "x="+m.x+"y="+m.y+" sel="+selectable+" selected=["
         * +selection.selection.length+"] arrow="+arrow+" cur="+cur_action +"
         * write="+writable);// debug
         */
        var x=$("#canvas").offset().left;
        var y=$("#canvas").offset().top;
        
        if(e.ctrlKey||dragging){
          if(m_.x-x<width/10) goLeft(10-10*(m_.x-x)/(width/10));
          if(m_.x-x>9*width/10) goRight(100*(m_.x-x-9*width/10)/(width));
          if(m_.y-y>9*height/10) goDown(100*(m_.y-y-9*height/10)/(height));
          if(m_.y-y<height/10) goUp(10-10*(m_.y-y)/(height/10));
        }
        last_event=e;
        
        if(!dragging) mode.contextual_mousemove(e);
        
        if(e.buttons==4){
          pan(e);
        }
        
      });
  
  // mousedown doit lancer un callback(?) qui teste si le truc s'éloigne avant
  // le mouseup (3px?)
  // si on s'éloigne, on lance drag()
  // mouseup doit regarder si on s'est éloigné, et sinon, lancer click(), si oui
  // fin_de_drag()
  // +tester le fucking dblclick
  
  $('#canvas').mousedown(function(e){ // what happens with a click
    e.preventDefault ? e.preventDefault() : e.returnValue=false;
    
    if(e.button==1){
      pan_begin(e);
      return;
    }
    
    if(!selection) return;
    start_real=new Point(m_.x, m_.y);
    start_canvas=m;
    console.log(e.timeStamp-last_mousedown_timestamp);
    if(e.timeStamp-last_mousedown_timestamp<300){
      mode.contextual_dblclick(e);
      dblclicking=1;
    }else{
      $("#canvas").mousemove(mouse_is_down);
    }
    last_mousedown_timestamp=e.timeStamp;
    md_selectable=selectable;
    md_arrow=arrow ? arrow.split('_').splice(1, 2) : null;
    if(md_arrow){
      md_arrow[0]=parseInt(md_arrow[0]);
      md_arrow[1]=parseInt(md_arrow[1]);
    }
    md_handle=handle;
  });
  
  $('#canvas').mouseup(function(e){
    e.preventDefault ? e.preventDefault() : e.returnValue=false;
    
    if(mode!=ModeText){
      window.writable=false;
    }
    
    if(e.button==1){
      pan_end(e);
      return;
    }
    
    if(!selection) return;
    end_real=new Point(m_.x, m_.y);
    if(dblclicking){
      dblclicking=0;
    }else{
      if(window.dragging){
        if(cur_action!=null) mode.contextual_end_drag(e);
        window.dragging=0;
      }else{
        mode.contextual_click(e);
      }
      $("#canvas").off('mousemove', mouse_is_down);
    }
    md_selectable=null;
    md_arrow=null;
    md_handle=null;
    cur_action=null;
    window.dragging=null; // should more aggressively tell that we are no more
    // dragging
  });// end mouseup
  
  // hack if mouse leaves the canvas [ :'-( ]
  $('#canvas').mouseleave(function(e){
    window.writableonmouseleave=window.writable;
    if(!selection) return;
    window.writable=true;// activate ability to write and use browser shortcuts
    e.preventDefault ? e.preventDefault() : e.returnValue=false;
    end_real=new Point(m_.x, m_.y);
    $("#canvas").off('mousemove', mouse_is_down);// if needed
    
    if(dblclicking){
      dblclicking=0;
    }else{
      if(window.dragging){
        mode.contextual_end_drag(e);
        window.dragging=0;
      }
    }
    window.dragging=null;// should more aggressively tell that we are no more
    // dragging
  });
  
  $('#canvas').mouseenter(function(e){
    window.writable=window.writableonmouseleave||mode==ModeText;
  });
  
});// end
