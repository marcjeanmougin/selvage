"use strict";
/**
 * comments = see mouse.js
 * 
 * This file is calling svg/path.js
 * 
 */

// bezier
var ModeBezier=new Mode();
ModeBezier.startPathHandle;

// ModeBezier.createPath=function(p0){};
// ModeBezier.addSegment=function(){};
// ModeBezier.endPath=function(b){};

ModeBezier.menu="Bezier mode "
    +"<a href='#' onclick=ModeBezier.prefs.type='bezier';><img src='img/bezier.svg'/></a>"
    +"<a href='#' onclick=ModeBezier.prefs.type='line';><img src='img/line.svg'/></a>"
    +"<a href='#' onclick=ModeBezier.prefs.type='paraxial';><img src='img/paraxial.svg'/></a>";
ModeBezier.cursor="url(img/cursors/cursorBezier.svg) 10 10";
ModeBezier.prefs={
  type: "bezier",
  dir: "H"
};

ModeBezier.contextual_mousemove=function(e){
  if(currently_drawn){
    if(this.temp) this.temp.remove(true);
    switch(this.prefs.type){
    case "bezier":
      this.temp=work.create(new Path("M"+this.lastPoint+"C"+this.lastctrlpt+" "
          +m+" "+m), true);
      break;
    case "line":
      this.temp=work.create(new Path("M"+this.lastPoint+"L"+this.lastctrlpt+" "
          +m+" "+m), true);
      break;
    case "paraxial":
      this.temp=work.create(new Path("M"+this.lastPoint+"L"+this.lastctrlpt+" "
          +m+" "+m), true);
      break;
    }
    this.temp.node.setAttribute("fill", "none");
    this.temp.node.setAttribute("stroke", "#f00");
  }
};

ModeBezier.contextual_start_drag=function(e){
  if(this.prefs.type!='bezier') return;
  cur_action="new cubic pt";
  if(currently_drawn){
    if(this.temp) this.temp.remove(true);
    this.temp=work.create(new Path("M"+this.lastPoint+"C"+this.lastctrlpt+" "
        +m.add(m.minus(start_canvas))+" "+start_canvas), true);
    this.temp.node.setAttribute("fill", "none");
    this.temp.node.setAttribute("stroke", "#f00");
    // start cubic bezier point creation (depending on context TODO )
  }
};

ModeBezier.contextual_drag=function(e){
  if(this.prefs.type!='bezier') return;
  
  if(this.l) this.l.remove(true);
  this.l=work.create(new Path("M"+start_canvas.minus(m.minus(start_canvas))+"L"
      +m), true);
  this.l.node.setAttribute("stroke", "#00f", true);
  if(!currently_drawn) return;
  if(this.temp) this.temp.remove(true);
  this.temp=work.create(new Path("M"+this.lastPoint+"C"+this.lastctrlpt+" "
      +start_canvas.add(start_canvas.minus(m))+" "+start_canvas), true);
  this.temp.node.setAttribute("fill", "none");
  this.temp.node.setAttribute("stroke", "#f00");
};

ModeBezier.contextual_end_drag=function(e){
  if(this.prefs.type!='bezier') return;
  
  if(this.l) this.l.remove(true);
  this.l=null;
  if(!currently_drawn){// new path drawing
    this.startPathHandle=ui.draw_point(start_canvas);
    currently_drawn=work.create(new Path("M"+start_canvas));
    this.temp=null;
    currently_drawn.set("stroke", $("#strokeColor").val());
    currently_drawn.set("fill", "none");
    currently_drawn.set("stroke-width", $("#strokeWidth").val());
  }else{
    // this.temp.remove();
    currently_drawn.C(this.lastctrlpt, start_canvas.add(start_canvas.minus(m)),
        start_canvas);
    if(this.startPathHandle.containsPoint(start_canvas)){
      currently_drawn.z();
      this.contextual_dblclick();
    }
  }
  // if(!this.lastPoint) this.lastPoint=new Point();
  this.lastPoint=start_canvas;
  this.lastctrlpt=m;
};

ModeBezier.contextual_click=function(){
  if(currently_drawn){
    switch(this.prefs.type){
    case "bezier":
      if(this.lastctrlpt!=this.lastPoint) currently_drawn.C(this.lastctrlpt, m,
          m);
      else currently_drawn.l(m.minus(this.lastPoint));
      break;
    case "line":
      currently_drawn.l(m.minus(this.lastPoint));
      break;
    case "paraxial":
      if(this.prefs.dir=="H"){
        currently_drawn.H(m);
        this.prefs.dir="V";
      }else{
        currently_drawn.V(m);
        this.prefs.dir="H";
      }
      break;
    }
    if(this.startPathHandle.containsPoint(m)){
      currently_drawn.z();
      this.contextual_dblclick();
    }
    this.lastPoint=m;
    this.lastctrlpt=m;
  }else{// start a new path drawing
    this.startPathHandle=ui.draw_point(start_canvas);
    currently_drawn=work.create(new Path("M"+m));
    this.lastPoint=m;
    this.lastctrlpt=m;
    // window.currently_drawn.attr({'fill':'none','stroke':'#000'});
    currently_drawn.set("fill", "none");
    currently_drawn.set("stroke", $("#strokeColor").val(), true);
    currently_drawn.set("stroke-width", $("#strokeWidth").val(), true);
  }
  
};

ModeBezier.contextual_dblclick=function(){
  this.startPathHandle.remove(true);
  if(!currently_drawn) return;
  currently_drawn.set("fill", "none");
  currently_drawn.set("stroke", $("#strokeColor").val());
  currently_drawn.set("stroke-width", $("#strokeWidth").val());
  if(this.temp) this.temp.remove(true);
  this.temp=null;
  selection.select(currently_drawn.id);
  // currently_drawn.style() //TODO
  currently_drawn.sel();
  // other fields ? center, etc
  // currently_drawn.D();
  currently_drawn=null;
};
