"use strict";
/*
 * Calling svg/{rect,ellipse,path}.js Called by mouse.js
 * 
 */
// rect
var ModeRect=new Mode();
ModeRect.cursor="url(img/cursors/cursorRect.svg) 10 10";
ModeRect.menu=" x=<input id='menu_x' style='width:50px' type='text' name='w' onchange='selection.changeX(this.value);'/>"
    +" y=<input id='menu_y' type='text' name='h'  style='width:50px' onchange='selection.changeY(this.value);'/>"
    +" w=<input id='menu_width' type='text'  style='width:50px' name='rx' onchange='selection.changeWidth(this.value);'/>"
    +" h=<input id='menu_height' style='width:50px' type='text' name='ry' onchange='selection.changeHeight(this.value)'/>"
// + " Rx=<input type='text' style='width:50px' name='rx' onchange='()'/>"
// + " Ry=<input style='width:50px' type='text' name='ry' onchange='()'/>"
;
ModeRect.contextual_start_drag=function(e){
  cur_action="draw_rect";
};

ModeRect.contextual_drag=function(e){
  if(currently_drawn) currently_drawn.remove(true);
  currently_drawn=work.create(new Rect(m, start_canvas), true);
  setColors(true);
};

ModeRect.contextual_end_drag=function(e){
  if(currently_drawn) currently_drawn.remove(true);
  
  currently_drawn=work.create(new Rect(m, start_canvas));
  currently_drawn.sel();
  currently_drawn.set("fill", $("#fillColor").val());
  currently_drawn.set("stroke", $("#strokeColor").val());
  currently_drawn.set("stroke-width", $("#strokeWidth").val());
  selection.select(currently_drawn.node.id);
  // currently_drawn.style() //TODO
  // other fields ? center, etc
  currently_drawn=null;
  
};

// ellipse
var ModeEllipse=new Mode();
ModeEllipse.cursor="url(img/cursors/cursorEllipse.svg) 10 10";
ModeEllipse.menu="Mode ellipse TODO";
ModeEllipse.contextual_start_drag=function(e){
  cur_action="draw_ellipse";
};
ModeEllipse.contextual_drag=function(e){
  if(currently_drawn) currently_drawn.remove(true);
  currently_drawn=work.create(new Ellipse(m, start_canvas), true);
  setColors(true);
};

ModeEllipse.contextual_end_drag=function(e){
  if(currently_drawn) currently_drawn.remove(true);
  currently_drawn=work.create(new Ellipse(m, start_canvas));
  currently_drawn.set("fill", $("#fillColor").val());
  currently_drawn.set("stroke", $("#strokeColor").val());
  currently_drawn.set("stroke-width", $("#strokeWidth").val());
  selection.select(currently_drawn.node.id);
  currently_drawn.sel();
  // currently_drawn.style() //TODO
  // other fields ? center, etc
  currently_drawn=null;
};

// star
var ModeStar=new Mode();

ModeStar.prefs={
  poly: false,
  n_pics: 5,
  star_ratio: 0.3333
};

ModeStar.cursor="url(img/cursors/cursorStar.svg) 10 10";

ModeStar.menu="<a href='#' onclick=ModeStar.prefs.poly=true;><img src='img/poly.svg'/></a>"
    +"<a href='#' onclick=ModeStar.prefs.poly=false;><img src='img/star.svg'/></a>"
    +"corners=<input style='width:50px' type='text' name='h' value="
    +ModeStar.prefs.n_pics
    +" onchange='ModeStar.prefs.n_pics=this.value;'/>"
    +"ratio=<input style='width:50px' type='text' value="
    +ModeStar.prefs.star_ratio
    +" name='h' onchange='ModeStar.prefs.star_ratio=this.value;'/>";

ModeStar.contextual_start_drag=function(e){
  cur_action="draw_star";
  // easier to redraw the star each time than to modify existing
};

ModeStar.contextual_drag=function(e){
  var pp=m.minus(start_canvas);
  if(currently_drawn) currently_drawn.remove(true);
  currently_drawn=work.create(new Star(pp, start_canvas, this.prefs), true);
  setColors(true);
};

ModeStar.contextual_end_drag=function(e){
  if(currently_drawn) currently_drawn.remove(true);
  currently_drawn=work.create(new Star(m.minus(start_canvas), start_canvas,
      this.prefs));
  currently_drawn.set("fill", $("#fillColor").val());
  currently_drawn.set("stroke", $("#strokeColor").val());
  currently_drawn.set("stroke-width", $("#strokeWidth").val());
  selection.select(currently_drawn.node.id);
  // currently_drawn.style() //TODO
  currently_drawn.sel();
  // other fields ? center, etc
  currently_drawn=null;
};

// spiral
var ModeSpiral=new Mode();
ModeSpiral.menu="Mode spiral TODO";
ModeSpiral.cursor="url(img/cursors/cursorSpiral.svg) 10 10";
ModeSpiral.contextual_start_drag=function(e){
};
ModeSpiral.contextual_drag=function(e){
};
ModeSpiral.contextual_end_drag=function(e){
};

// free hand
var ModeFreeHand=new Mode();
ModeFreeHand.startPathHandle;
ModeFreeHand.cursor="url(img/cursors/cursorFreehand.svg) 10 10";
ModeFreeHand.menu="Freehand mode. I may implement some smoothing, in a distant future";
ModeFreeHand.contextual_start_drag=function(e){
  this.startPathHandle=ui.draw_point(start_canvas);
  cur_action="draw_line";
  currently_drawn=work.create(new Path());
  currently_drawn.translate(start_canvas);
  // currently_drawn.style();
  currently_drawn.set("stroke", $("#strokeColor").val());
  currently_drawn.set("fill", "none");
  currently_drawn.set("stroke-width", $("#strokeWidth").val());
};

ModeFreeHand.contextual_drag=function(e){
  currently_drawn.l(m.minus(start_canvas));
  start_canvas=m;
};
ModeFreeHand.contextual_end_drag=function(e){
  if(this.startPathHandle.containsPoint(m)){
    currently_drawn.z();
  }
  this.startPathHandle.remove(true);
  selection.select(currently_drawn.node.id);
  // currently_drawn.style() //TODO
  currently_drawn.sel();
  // other fields ? center, etc
  currently_drawn=null;
};
