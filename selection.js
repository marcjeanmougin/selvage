"use strict";
/*
 * Selection handling. mostly an array selection.selection with id's of selected
 * objects.
 */

//
function Selection(ui){
  
  this.selection=[];
  this.ui=ui;
  ui.selection=this;
  this.selBox=new Box();
  this.center=new Point();
};

Selection.prototype=new Manipulable();

Selection.prototype.constructor=Selection;

Selection.prototype.has=function(id){
  return (($.inArray(id, this.selection))!=-1);
};

Selection.prototype.select=function(s){
  s=s||selectable;
  this.clear();
  return this.add(s);
};

Selection.prototype.clear=function(){
  this.ui.clearSelection();
  this.selection=[];
  // console.log("clear", this.ui);
  this.selBox=new Box();
};

Selection.prototype.update=function(){
  this.center=new Point(this.selBox.x+this.selBox.width/2, this.selBox.y
      +this.selBox.height/2);
  $("#menu_x").val(this.selBox.x);
  $("#menu_y").val(this.selBox.y);
  $("#menu_width").val(this.selBox.width);
  $("#menu_height").val(this.selBox.height);
};

Selection.prototype.add=function(id){
  var x=get(id);
  if(!id||(($.inArray(id, this.selection))!=-1)||!(x.selectable)) return null;
  this.selection.push(id);
  this.ui.selectObj(id);
  if(this.selection.length>1){
    this.selBox=this.selBox.merge(get(id).box());
  }else{
    // console.log(x.node.getAttribute("fill"));
    $("#fillColor").val(x.get("fill")||"black");
    $("#strokeColor").val(x.get("stroke")||"none");
    $("#strokeWidth").val(x.get("stroke-width")||"1");
    var l=$("#fillColor").val().match(/(ijslinearGradient.*)"\)/);
    if(l) Colors.editLGradient(l[1]);
    var l=$("#fillColor").val().match(/(ijsradialGradient.*)"\)/);
    if(l) Colors.editRGradient(l[1]);
    this.selBox=get(id).box();
  }
  this.update();
  return this;
};

Selection.prototype.minus=function(id){
  if(!this.has(id)) return;
  var x=this.selection.indexOf(id);
  this.selection.splice(x, 1);
  this.ui.unselectObj(id);
  this.applyMatrix(new Matrix());// recomputes selBox
};

Selection.prototype.moveLeft=function(){
  this.translate(new Point(-20/vb.zoom, 0));
  Arrows();
};
Selection.prototype.moveRight=function(){
  this.translate(new Point(20/vb.zoom, 0));
  Arrows();
};
Selection.prototype.moveUp=function(){
  this.translate(new Point(0, -20/vb.zoom));
  Arrows();
};
Selection.prototype.moveDown=function(){
  this.translate(new Point(0, 20/vb.zoom));
  Arrows();
};

// Suppr, NOT unselect. see minus.
Selection.prototype.remove=function(){
  this.selection.forEach(function(elt,i){
    get(elt).remove();
  });
  this.clear();
  Arrows.remove();
};

Selection.prototype.fill=function(f){
  this.selection.forEach(function(elt){
    // get(elt).set("fill", f);
    get(elt).style("fill", f);
  });
  $("#fillColor").val(f);
};

Selection.prototype.stroke=function(s){
  this.selection.forEach(function(elt){
    // get(elt).set("stroke", s);
    get(elt).style("stroke", s);
  });
};

Selection.prototype.strokeWidth=function(s){
  this.selection.forEach(function(elt){
    // get(elt).set("stroke-width", s);
    get(elt).style("strokeWidth", s);
  });
};

Selection.prototype.applyMatrix=function(m,bo){
  if(!this.selection.length) return;
  // var b=get(this.selection[0]).box()
  this.selection.forEach(function(id,i){
    get("ui_"+id).remove(true);
    var a=get(id);
    a.applyMatrix(m, bo);
    selection.ui.selectObj(id);
    if(!i) b=a.box();
    else b=b.merge(a.box());
  });
  this.selBox=b;
  this.update();
};

Selection.prototype.sorta=function(a,b){
  a=get(a);
  b=get(b);
  var ans=a.position()-b.position();
  console.log(a, b, ans);
  return ans;
};
Selection.prototype.sortd=function(a,b){
  return this.sorta(b, a);
};

Selection.prototype.forward=function(){
  this.selection=this.selection.sort(this.sorta);
  this.selection.forEach(function(id){
    get(id).forward();
  });
};

Selection.prototype.backward=function(){
  this.selection=this.selection.sort(this.sortd);
  this.selection.forEach(function(id){
    get(id).backward();
  });
};

Selection.prototype.front=function(){
  this.selection=this.selection.sort(this.sorta);
  this.selection.forEach(function(id){
    get(id).front();
  });
};

Selection.prototype.back=function(){
  this.selection=this.selection.sort(this.sortd);
  this.selection.forEach(function(id){
    get(id).back();
  });
};

Selection.prototype.changeWidth=function(w,b){
  if(this.selBox.width==0) return;
  this.scale(w/this.selBox.width, 1, null, b);
  Arrows();
};

Selection.prototype.changeHeight=function(h,b){
  if(this.selBox.width==0||this.selBox.height==0) return;
  this.scale(1, h/this.selBox.height, null, b);
  Arrows();
};

Selection.prototype.changeX=function(x,b){
  this.translate(new Point(x-this.selBox.x, 0), b);
  Arrows();
};

Selection.prototype.changeY=function(y,b){
  this.translate(new Point(0, y-this.selBox.y), b);
  Arrows();
};

Selection.prototype.duplicate=function(){
  this.selection.forEach(function(id){
    get(id).duplicate();
  });
};
Selection.prototype.duplicate2=function(dest){
  this.selection.forEach(function(id){
    get(id).duplicate2(false, dest);
  });
};

Selection.prototype.clone=function(){
  this.selection.forEach(function(id){
    work.create(new Use("#"+id)).sel();
  });
};
