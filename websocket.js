"use strict";
/*
 * Main code for client/server interaction
 * 
 * Make sure all functions are properly defined before initalizing the websocket
 * Websocket itself is defined in userSettings.js
 */

var verbose=false;
function send(data){
  var it=JSON.stringify(data);
  if(verbose) console.log('sending'+it);
  ws.send(''+it);
};

ws.onopen=function(){
  var drawing=window.location.search.split('=')[1];
  if(!drawing){
    chooseDrawing();
  }else{
    loadDrawing(drawing);
  }
};

ws.onerror=function(){
  alert("you are now offline");
};

ws.onmessage=function(e){
  if(verbose) console.log(e.data);
  
  var d=JSON.parse(e.data);
  switch(d.type){
  case "setAttribute":
    if(!get(d.id)) console.log(e);
    get(d.id).set(d.attribute, d.value, true);
    if(d.attribute=="d") get(d.id).d2s();
    break;
  case "edit":
    // get(d.id).text(d.value, true);
    break;
  case "move":
    get(d.objParent).add(get(d.id), d.objPosition);
    if(d.objParent==work.node.id){
      get(d.id).sel();
    }else{
      get(d.id).unsel();
    }
    break;
  case "new": // create[where,what,id,args]
    var n=0;
    var toCreate=createObject(d.objType, d.objArgs);
    n=get(d.objParent).create(toCreate, true);
    if(d.objType=="textNode") return; // textNodes have no ID
    if(d.objParent==work.node.id) n.sel();
    n.node.setAttribute("id", d.objId, true);
    if(d.objType=='linearGradient'||d.objType=='radialGradient'){
      Colors.refreshList();
    }
    break;
  case "delete": // delete[id]
    get(d.id).remove(true);
    break;
  case "copy": // delete[id]
    Manipulable.prototype.duplicate.call(get(d.id), true, d.objId);
    break;
  case "applyMatrix":// applymatrix[id,m]
    m=d.matrix;
    get(d.id).matrix=new Matrix(m.a, m.b, m.c, m.d, m.e, m.f);
    get(d.id).node.setAttribute('transform', 'matrix('+m.a+','+m.b+','+m.c+','
        +m.d+','+m.e+','+m.f+')');
    break;
  }
};
