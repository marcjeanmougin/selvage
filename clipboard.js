"use strict";
/*
 * The cliboard is just a group in the defs, where we copy what was selected
 * during a ^C/^X and from where we copy things when a ^V occurs
 */

var Clipboard={};

Clipboard.paste=function(){
  clipboard.duplicate2(false, work.node.id).ungroup();
};
Clipboard.copy=function(){
  clipboard.each(function(i,elt){
    elt[i].remove();
  });
  selection.duplicate2(clipboard.node.id);
};
Clipboard.cut=function(){
  this.copy();
  selection.remove();
};

var clipboard;
