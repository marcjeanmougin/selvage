"use strict";
/*
 * Initializer functions.
 * 
 */

var width;
var height;

var draw;// SVG
var vb;

var work;
var selection;
var ui_artifacts;
var ui_arrows_group;

var selectable=null;
var arrow=null;
var handle=null;
var mode;
var ui;
var b;

function popup(id){
};

var ModePathEdit, ModeStandard, ModeRect, ModeEllipse, ModeStar, ModeSpiral, ModeFreeHand, ModeBezier, ModeText;

var Modes={
  'standard': ModeStandard,
  'pathEdit': ModePathEdit,
  'zoom': ModeZoom,
  'rect': ModeRect,
  'ellipse': ModeEllipse,
  'star': ModeStar,
  'spiral': ModeSpiral,
  'freeHand': ModeFreeHand,
  'bezier': ModeBezier,
  'text': ModeText,
};

function initView(event){
  if(!draw) return;
  width=$("#canvas").width();
  height=$("#canvas").height();
  center_real=new Point(width/2+20, height/2+40);
  draw.node.setAttribute("width", width);
  draw.node.setAttribute("height", height);
  draw.viewbox(vb.x, vb.y, width, height);
  vb=draw.viewbox();
};
window.onresize=initView;

function loadDrawing(drawing){
  if(window.location.search!="?d="+drawing){
    window.location.search="?d="+drawing;
  }
  $.cookie("drawing", drawing);
  send({
    type: 'open',
    drawing: drawing
  });
  $('#show_id').html(drawing);
  if(draw) $("#drawingSvg").remove();
  draw=new SVG("canvas");
  draw.viewbox(0, 0, 100, 100);
  vb=draw.viewbox();
  initView();
  
  // unsure.
  work=draw.create(new Group, true);
  work.node.setAttribute("id", "root");
  ui_artifacts=draw.create(new Group, true);
  ui_arrows_group=ui_artifacts.create(new Group, true);
  var CBP=work.create(new Defs);
  var CB;
  if(!(CB=get("CB"))){
    CB=CBP.create(new Defs);
    CB.set("id", "CB");
  }
  clipboard=CB.create(new Group);
  
  ui=new UI();
  window.selection=new Selection(ui);
  changeMode('standard');
  writable=false;
  
};
SvgObject.prototype.s__=function(e){
  selectable=this.id;
  e.stopPropagation();
  // console.log("selected");
};

SvgObject.prototype.sel=function(){
  this.selectable=true;
  // mouseenter does not work in chrome
  // this.on("mouseenter", this.s__);
  this.on("mouseover", this.s__);
  // worked around the following:
  // this.on("mouseleave", function(){
  // selectable=null;
  // });
};
var deselect=function(e){
  selectable=null;
  window.arrow=null;
  window.handle=null;
  // console.log("deselected");
};

SvgObject.prototype.unsel=function(){
  this.selectable=null;
  // this.off("mouseenter", this.s__);
  this.off("mouseover", this.s__);
  // this.off("mouseenter", function(){
  // selectable=null;
  // });
};

function chooseDrawing(){
  var drawing=$('#show_id').html();
  if(!drawing) drawing=Math.random().toString(36).substring(7);
  $('#drawingName').val(drawing);
  $("#startDialog").overlay({
    load: true, // TODO: why is this needed?
    closeOnClick: draw
  // TODO: why doesn't this work?
  }).load();
};

var resize_panel_start;
function resize_panel(e){
  e.preventDefault ? e.preventDefault() : e.returnValue=false;
  // console.log(e.pageX);
  $("#panels").css("width", "+="+(resize_panel_start-e.pageX));
  $("#canvas").css("width", "-="+(resize_panel_start-e.pageX));
  resize_panel_start=e.pageX;
};

$(function(){
  $("ul.tabs").tabs("div.panes > div");
  
  $('#canvas').mouseover(deselect);
  
  $(".dropdown dt a").click(function(){
    $(this).parent().parent().find('ul').toggle();
  });
  $("#resizing").on('mousedown', function(e){
    resize_panel_start=e.pageX;
    e.preventDefault ? e.preventDefault() : e.returnValue=false;
    console.log("drag");
    $(window).on('mousemove', resize_panel);
    $(window).one('mouseup', function(e){
      e.preventDefault ? e.preventDefault() : e.returnValue=false;
      $(window).off('mousemove', resize_panel);
      initView();
    });
  });
  
  $("#menus>div").on('click', function(e){
    $(this).find("ul").toggle();
  });
  
  $(':file').change(function(){
    reader.readAsText(this.files[0]);
  });
});

var reader=new FileReader();
reader.onloadend=function(){
  var x=$.parseXML(this.result);
  Load.process(x);
};
