"use strict";
/*
 * Keyboard gestion. All the key events should be defined here
 * 
 */

var writable=true;

$(function(){
  
  $(window).keydown(function(e){
    
    if(!window.writable){
      e.preventDefault ? e.preventDefault() : e.returnValue=false;
    }
    if(window.writable) return;
    
    var intKeyCode=e.keyCode;
    var intAltKey=e.altKey||e.metaKey;
    var intCtrlKey=e.ctrlKey;
    var intShiftKey=e.shiftKey;
    switch(intKeyCode){
    
    case 36: // home
      selection.front();
      break;
    case 35: // end
      selection.back();
      break;
    case 33: // pgup
      selection.forward();
      break;
    case 34: // pgdwn
      selection.backward();
      break;
    
    case (37): // left
      selection.moveLeft();
      break;
    case (38): // up
      selection.moveUp();
      break;
    case (39): // right
      selection.moveRight();
      break;
    case (40): // down
      selection.moveDown();
      break;
    case (107):
    case (187): // +
      zoomIn();
      break;
    case (109):
    case (189): // -
      zoomOut();
      break;
    case 46: // Suppr
      selection.remove();
      break;
    case 65:// A
      if(intCtrlKey){
        work.each(function(x,e){
          selection.add(e[x].node.id);
        });
      }
      break;
    case 67:// C
      if(intCtrlKey){
        Clipboard.copy();
      }
      break;
    case 88:// X
      if(intCtrlKey){
        Clipboard.cut();
      }
      break;
    case 68:// D
      if(intCtrlKey){
        selection.duplicate();
      }else if(intAltKey){
        selection.clone();
      }
      break;
    case 71:// g
      if(!intCtrlKey) return;
      if(intShiftKey){
        // ungroup
        selection.selection.forEach(function(id){
          var group;
          if((group=get(id)).type=="group"){
            group.ungroup();
          }
        });
      }else{
        // group
        var g=work.create(new Group);
        selection.selection.forEach(function(id){
          var e=get(id);
          e.unsel();
          g.add(e);
          send({
            type: "move",
            id: e.node.id,
            objParent: g.node.id
          });
        });
        selection.clear();
        selection.add(g.node.id);
        g.sel();
      }
      break;
    case 83:// S
      if(intCtrlKey){
        send({
          type: "save",
        });
      }
      break;
    case 72:// h
      selection.flipH();
      break;
    case 86:// v
      if(intCtrlKey){
        Clipboard.paste();
      }else{
        selection.flipV();
      }
      break;
    case 161:// ! (invert selection)
      var fs=[];
      work.each(function(x,e){
        selectable=e[x].node.id;
        if(!selection.has(selectable)) fs.push(selectable);
      });
      selection.clear();
      fs.forEach(function(a){
        selection.add(a);
      });
      break;
    default:
      console.log("kb", intKeyCode, e);
      // alert(intKeyCode);
      
    } // end switch
    
  });
});
