"use strict";
//
function Arrow(dir,m){
  this.m=m;
  this.direction=dir;
  var p, d;
  var b=selection.selBox;
  if(b.width==0) return null;
  
  var h="m"+(10/vb.zoom)+" 0v"+(5/vb.zoom)+"h"+(10/vb.zoom)+"v"+(-5/vb.zoom)
      +"l"+(10/vb.zoom)+" "+(10/vb.zoom)+""+(-10/vb.zoom)+" "+(10/vb.zoom)+"v"
      +(-5/vb.zoom)+"h"+(-10/vb.zoom)+"v"+(5/vb.zoom)+"l"+(-10/vb.zoom)+""
      +(-10/vb.zoom)+"z";// --
  var v="m"+(10/vb.zoom)+" 0l"+(10/vb.zoom)+" "+(10/vb.zoom)+" h"+(-5/vb.zoom)
      +"v"+(10/vb.zoom)+"h"+(5/vb.zoom)+"l"+(-10/vb.zoom)+" "+(10/vb.zoom)+""
      +(-10/vb.zoom)+""+(-10/vb.zoom)+"h"+(5/vb.zoom)+"v"+(-10/vb.zoom)+"h"
      +(-5/vb.zoom)+"z";// |
  var z="m"+(15/vb.zoom)+" 0h"+(15/vb.zoom)+"v"+(15/vb.zoom)+"l"+(-5/vb.zoom)
      +""+(-5/vb.zoom)+""+(-15/vb.zoom)+" "+(15/vb.zoom)+" "+(5/vb.zoom)+" "
      +(5/vb.zoom)+"h"+(-15/vb.zoom)+"v"+(-15/vb.zoom)+"l"+(5/vb.zoom)+" "
      +(5/vb.zoom)+" "+(15/vb.zoom)+""+(-15/vb.zoom)+""+(-5/vb.zoom)+""
      +(-5/vb.zoom)+"z"; // /
  var s="h"+(15/vb.zoom)+"l"+(-5/vb.zoom)+" "+(5/vb.zoom)+" "+(15/vb.zoom)+" "
      +(15/vb.zoom)+" "+(5/vb.zoom)+""+(-5/vb.zoom)+"v"+(15/vb.zoom)+"h"
      +(-15/vb.zoom)+"l"+(5/vb.zoom)+""+(-5/vb.zoom)+""+(-15/vb.zoom)+""
      +(-15/vb.zoom)+""+(-5/vb.zoom)+" "+(5/vb.zoom)+"z";// \
  
  function getArrowCoord(dir,size){
    return (dir<0 ? -30/vb.zoom : (dir>0 ? size : size/2-15/vb.zoom));
  }
  
  p=new Point(b.x+getArrowCoord(dir[0], b.width), b.y
      +getArrowCoord(dir[1], b.height));
  d="M"
      +p
      +(dir[0]&&dir[1] ? (xor(dir[0]*dir[1]>0, m) ? z : s)
          : (xor(dir[0], m) ? v : h));
  Path.call(this, d);
  this.node.setAttribute("fill", "#000");
  this.node.setAttribute("stroke", "#fff");
  this.node.setAttribute("stroke-width", 0.5/vb.zoom);
  this.node.setAttribute("id", "arrow_"+dir.join('_'));
  this.on("mouseover", function(e){
    window.arrow=this.id;
    e.stopPropagation();
  });
  // this.on("mouseleave", function(){
  // window.arrow=null;
  // });
}

Arrow.prototype=new Path("M10 0v5h10v-5l10 10-10 10v-5h-10v5l-10-10z");// <=>
Arrow.prototype.contructor=Arrow;

function Arrows(){
  if(mode!=ModeStandard) return;
  var m=ModeStandard.m;
  if(ui.arr){
    Arrows.remove();
  }
  if(selection.selection.length==0) return;
  ui.arr=[];
  for( var dx=-1; dx<=1; dx++)
    for( var dy=-1; dy<=1; dy++)
      if(dx||dy) ui.arr.push(ui_arrows_group.uiArrow([dx, dy], m));
}

Arrows.remove=function(){
  ui_arrows_group.remove(true);
  ui_arrows_group=ui_artifacts.create(new Group, true);
  ui.arr=null;
};
Arrows.toogle=function(){
  ModeStandard.m=1-ModeStandard.m;
  Arrows();
};

function arrow_drag_start(){
};

function arrow_drag(){
  
  function getAngle(x,y,m){
    return m ? new Angle(0) : x.minus(selection.center).angle().minus(
        y.minus(selection.center).angle());
  }
  
  var d=m.minus(start_canvas);
  var s=selection.selBox;
  
  if(ModeStandard.m){
    selection.scale(md_arrow[0] ? (s.width+md_arrow[0]*d.x)/s.width : 1,
        md_arrow[1] ? (s.height+md_arrow[1]*d.y)/s.height : 1,
        new Point(s.x+(md_arrow[0]<0 ? s.width : 0), s.y
            +(md_arrow[1]<0 ? s.height : 0)), true);
    md_arrow[0]*=signb((s.width+md_arrow[0]*d.x));
    md_arrow[1]*=signb((s.height+md_arrow[1]*d.y));
  }else{
    if(md_arrow[0]&&md_arrow[1]){
      selection.rotate(getAngle(m, start_canvas, 0), null, true);
    }else{
      selection.skew(getAngle(start_canvas, m, md_arrow[0]), getAngle(m,
          start_canvas, md_arrow[1]), null, true);
    }
  }
  start_canvas=m;
};

function arrow_drag_end(){
  arrow=null;
  selection.translate(new Point());
};
