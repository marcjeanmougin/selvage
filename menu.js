"use strict";
var Menu={};

Menu.File={};
Menu.File._new=function(){// keyword
  loadDrawing(Math.random().toString(36).substring(7));
};
Menu.File.open=function(){
  chooseDrawing();
};
Menu.File.link=function(){
  $('#imageDialog').overlay({
    load: true,
    mask: true
  }).load();
};
Menu.File.import=function(){
  $('#importDialog').overlay({
    load: true,
    mask: true
  }).load();
};
Menu.File.save=function(){
  send({
    type: "save",
  });
};
Menu.File.prop=function(){
  // maybe define the viewbox rect when exporting ?
};

Menu.Edit={};
Menu.Edit.copy=function(){
  Clipboard.copy();
};
Menu.Edit.cut=function(){
  Clipboard.cut();
};
Menu.Edit.paste=function(){
  Clipboard.paste();
};
Menu.Edit.duplicate=function(){
  selection.duplicate();
};
Menu.Edit.clone=function(){
  selection.clone();
};
Menu.Edit.selectAll=function(){
};
Menu.Edit.invertSelection=function(){
};

Menu.View={};
Menu.View.zoomIn=function(){
};
Menu.View.zoomOut=function(){
};

Menu.Object={};
Menu.Object.group=function(){
};
Menu.Object.ungroup=function(){
};

Menu.Path={};

Menu.Help={};
Menu.Help.about=function(){
  $('#aboutDialog').overlay({
    load: true,
    mask: true
  }).load();
};
