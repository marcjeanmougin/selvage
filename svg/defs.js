"use strict";
/*
 * SVG allows graphical objects to be defined for later reuse. To do this, it
 * makes extensive use of IRI references [RFC3987] to these other objects. For
 * example, to fill a rectangle with a linear gradient, you first define a
 * ‘linearGradient’ element and give it an ID, as in: <linearGradient
 * id="MyGradient">...</linearGradient>
 * 
 * Some types of element, such as gradients, will not by themselves produce a
 * graphical result. They can therefore be placed anywhere convenient. However,
 * sometimes it is desired to define a graphical object and prevent it from
 * being directly rendered. it is only there to be referenced elsewhere. To do
 * this, and to allow convenient grouping defined content, SVG provides the
 * ‘defs’ element.
 * 
 * 
 * It is recommended that, wherever possible, referenced elements be defined
 * inside of a ‘defs’ element. Among the elements that are always referenced:
 * ‘altGlyphDef’, ‘clipPath’, ‘cursor’, ‘filter’, ‘linearGradient’, ‘marker’,
 * ‘mask’, ‘pattern’, ‘radialGradient’ and ‘symbol’. Defining these elements
 * inside of a ‘defs’ element promotes understandability of the SVG content and
 * thus promotes accessibility.
 */

function Defs(){
  Container.call(this, create("defs"));
  this.type="defs";
};

Defs.prototype=new Container();
Defs.prototype.constructor=Defs;
