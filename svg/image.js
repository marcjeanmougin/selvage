"use strict";
/*
 * The ‘image’ element indicates that the contents of a complete file are to be
 * rendered into a given rectangle within the current user coordinate system.
 * 
 * Unlike ‘use’, the ‘image’ element cannot reference elements within an SVG
 * file.
 * 
 */
//
function Image(where,p1,p2){
  SvgObject.call(this, create("image"));
  this.type="image";
  this.setWhere(where);
  if(!p2){
    console.log('aa');
    p1=new Point();
    // thanks stackoverflow 3877027 & 10338613
    $("<img/>").attr("src", where).one('load', function(){
      Image.width=this.width;
      Image.height=this.height;
    }).each(function(){
      if(this.complete) $(this).load();
    });
    p2=new Point(Image.width||100, Image.height|100);
  }
  
  this.node.setAttribute("width", Math.abs(p1.x-p2.x));
  this.node.setAttribute("height", Math.abs(p1.y-p2.y));
  this.node.setAttribute("x", Math.min(p1.x, p2.x));
  this.node.setAttribute("y", Math.min(p1.y, p2.y));
};
Image._width=100;
Image._height=100;

Image.prototype=new SvgObject();
Image.prototype.constructor=Image;
Image.prototype.getArgs=function(){
  return [this.node.getAttributeNS(NS_XLINK, "xlink:href")].concat(this
      .pointsFromCoords());
};

Image.prototype.setWhere=function(where){
  this.node.setAttributeNS(NS_XLINK, "xlink:href", where);
};
