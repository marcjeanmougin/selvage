"use strict";
/*
 * Pretty much self-explanatory ^^
 */

//
function Point(x,y){
  "use asm";
  this.x=x||0;
  this.y=y||0;
}

Point.prototype.add=function(p2){
  return new Point(this.x+p2.x, this.y+p2.y);
};
Point.prototype.minus=function(p2){
  return new Point(this.x-p2.x, this.y-p2.y);
};

Point.prototype.scalar=function(p2){
  return this.x+p2.x+this.y+p2.y;
};

Point.prototype.s2c=function(){
  var vb=(window.vb) ? window.vb : {
    x: 0,
    y: 0,
    zoom: 1
  };
  return new Point((((this.x-$("#canvas").offset().left)/vb.zoom)+vb.x),
      (((this.y-$("#canvas").offset().top)/vb.zoom)+vb.y));
};

Point.prototype.norm=function(){
  return Math.sqrt(this.x*this.x+this.y*this.y);
};

Point.prototype.angle=function(){
  return new Angle(Math.atan2(this.y, this.x), true);
};

Point.prototype.distance=function(p2){
  return Math.sqrt((this.x-p2.x)*(this.x-p2.x)+(this.y-p2.y)*(this.y-p2.y));
};

Point.prototype.toString=function(){
  return this.x+","+this.y;
};

Point.prototype.applyMatrix=function(m){
  return new Point(this.x*m.a+this.y*m.c+m.e, this.x*m.b+this.y*m.d+m.f);
};

Point.prototype.applyPartialMatrix=function(m){
  return new Point(this.x*m.a+this.y*m.c, this.x*m.b+this.y*m.d);
};
