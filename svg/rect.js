"use strict";
/*
 * Basic rect class (rect from p1 to p2)
 */

//
function Rect(p,p2){
  SvgObject.prototype.constructor.call(this, create('rect'));
  this.type="rect";
  if(!p2) return this;
  this.node.setAttribute("width", Math.abs(p.x-p2.x));
  this.node.setAttribute("height", Math.abs(p.y-p2.y));
  this.node.setAttribute("x", Math.min(p.x, p2.x));
  this.node.setAttribute("y", Math.min(p.y, p2.y));
};

Rect.prototype=new SvgObject();
Rect.prototype.constructor=Rect;
// Rect.prototype.constructor=Rect;
Rect.prototype.getArgs=function(){
  return this.pointsFromCoords();
};

Rect.prototype.containsPoint=function(p){
  var a=this.getArgs();
  console.log(this, p, ans, a);
  var ans=(a[0].x<=p.x)&&(a[0].y<=p.y)&&(a[1].x>=p.x)&&(a[1].y>=p.y);
  return ans;
};
