"use strict";
/*
 * z-order, listeners, and other stuff
 */

//
function SvgObject(node){
  Manipulable.call(this);
  this.type="svgobject";// should not happen
  if(this.node=node){
    this.node.instance=this;
  }
};
// inheritance
SvgObject.prototype=new Manipulable();
SvgObject.prototype.constructor=SvgObject;

SvgObject.prototype.box=function(){
  return new Box(this);
};

SvgObject.prototype.on=function(event,listener){
  this.node.addEventListener(event, listener, false);
};

SvgObject.prototype.off=function(event,listener){
  this.node.removeEventListener(event, listener, false);
};
SvgObject.prototype.applyMatrix=function(m,b){
  this.matrix=m.multiply(this.matrix);
  m=this.matrix;
  if(!b) send({
    type: "applyMatrix",
    id: this.node.id,
    matrix: m
  });
  this.node.setAttribute('transform', 'matrix('+m.a+','+m.b+','+m.c+','+m.d+','
      +m.e+','+m.f+')');
};

SvgObject.prototype.set=function(a,b,bo){
  if(a==null||b==null) return;
  // console.log(a, b);
  if(StylingProperties.indexOf(a)>=0) return this.style(dashToCamel(a), b, bo);
  if(!bo) send({
    type: "setAttribute",
    id: this.node.id,
    attribute: a,
    value: b
  });
  if(a=="xlink:href") this.node.setAttributeNS(NS_XLINK, a, b);
  else this.node.setAttribute(a, b);// Do NOT put this before (think a="id" ...)
  return this;
};

SvgObject.prototype.get=function(a){
  if(StylingProperties.indexOf(a)>=0){ return this.node.style[dashToCamel(a)]; }
  return this.node.getAttribute(a);
};

SvgObject.prototype.style=function(a,b,bo){
  if(a==null) return;
  // TODO proper escaping (including already escaped quotes)
  eval("this.node.style."+a+" =\""+b.replace('"', '', 'g')+"\"");
  if(!bo) send({
    type: "setAttribute",
    id: this.node.id,
    attribute: "style",
    value: this.node.getAttribute("style")
  });
  return this;
};

SvgObject.prototype.remove=function(b){
  if(!b) send({
    type: "delete",
    id: this.node.id,
  });
  if(this.parent) this.parent.removeElement(this);
};

SvgObject.prototype.siblings=function(){
  return this.parent.children();
};

SvgObject.prototype.position=function(){
  return this.siblings().indexOf(this);
};

SvgObject.prototype.next=function(){
  return this.siblings()[this.position()+1];
};

SvgObject.prototype.previous=function(){
  return this.siblings()[this.position()-1];
};

SvgObject.prototype.forward=function(b){
  var i=this.position();
  if(!b) send({
    type: "move",
    id: this.node.id,
    objParent: this.parent.node.id,
    objPosition: i+1
  });
  return this.parent.removeElement(this).put(this, i+1);
};

SvgObject.prototype.backward=function(b){
  var i=this.position();
  if(!b) send({
    type: "move",
    id: this.node.id,
    objParent: this.parent.node.id,
    objPosition: (i-1>=0) ? (i-1) : 0
  });
  if(i>0) this.parent.removeElement(this).add(this, i-1);
  return this;
};

SvgObject.prototype.front=function(b){
  if(!b) send({
    type: "move",
    id: this.node.id,
    objParent: this.parent.node.id,
    objPosition: -1
  });
  return this.parent.removeElement(this).put(this);
};

SvgObject.prototype.back=function(b){
  if(!b) send({
    type: "move",
    id: this.node.id,
    objParent: this.parent.node.id,
    objPosition: 0
  });
  var x=this.parent;
  x.removeElement(this);
  x.add(this, 0);
  return this;
};

SvgObject.prototype.getArgs=function(){
  return null;
};

SvgObject.prototype.pointsFromCoords=function(){
  var x1=parseFloat(this.node.getAttribute('x'));
  var y1=parseFloat(this.node.getAttribute('y'));
  var x2=x1+parseFloat(this.node.getAttribute('width'));
  var y2=y1+parseFloat(this.node.getAttribute('height'));
  return [new Point(x1, y1), new Point(x2, y2)];
};
