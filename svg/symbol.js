"use strict";
/*
 * The key distinctions between a ‘symbol’ and a ‘g’ are:
 * 
 * A ‘symbol’ element itself is _not_ rendered. Only instances of a ‘symbol’
 * element (i.e., a reference to a ‘symbol’ by a ‘use’ element) are rendered.
 * 
 * A ‘symbol’ element has attributes ‘viewBox’ and ‘preserveAspectRatio’ which
 * allow a ‘symbol’ to scale-to-fit within a rectangular viewport defined by the
 * referencing ‘use’ element.
 * 
 * Closely related to the ‘symbol’ element are the ‘marker’ and ‘pattern’
 * elements.
 */

function Symbol(){
  Container.call(this, create("symbol"));
  this.type="symbol";
};

Symbol.prototype=new Container();
Symbol.prototype.constructor=Symbol;
