"use strict";
/*
 * Manipulation functions for all objects Most are calling applyMatrix
 */
function Manipulable(){
  this.center=new Point();
  this.matrix=new Matrix();
  // this.box=null;
};

Manipulable.prototype.applyMatrix=function(m){
  return;
};

Manipulable.prototype.translate=function(p,b){
  this.applyMatrix(new Matrix(1, 0, 0, 1, p.x, p.y), b);
};

Manipulable.prototype.setCenter=function(c){
  this.center=c;
};

Manipulable.prototype.rotate=function(a,c,b){
  if(c==null) c=this.center;
  this.translate(new Point(-c.x, -c.y), b);
  this.applyMatrix(new Matrix(a.cos(), a.sin(), -a.sin(), a.cos(), 0, 0), b);
  this.translate(c, b);
};

Manipulable.prototype.scale=function(x,y,c,b){
  if(x==0) return;// x=0.001;
  if(y==0) return;// y=0.001;
  if(c==null) c=this.center;
  this.translate(new Point(-c.x, -c.y), b);
  this.applyMatrix(new Matrix(x, 0, 0, y, 0, 0), b);
  this.translate(c, b);
};

Manipulable.prototype.scaleX=function(x,c,b){
  this.scale(x, 1, c, b);
};
Manipulable.prototype.scaleY=function(y,c,b){
  this.scale(1, y, c, b);
};

Manipulable.prototype.skew=function(x,y,c,b){
  if(c==null) c=this.center;
  this.translate(new Point(-c.x, -c.y), b);
  this.applyMatrix(new Matrix(1, (y.tan()), (x.tan()), 1, 0, 0), b);
  this.translate(c, b);
};

Manipulable.prototype.skewX=function(x,c,b){
  this.skew(x, new Angle(0), c, b);
};
Manipulable.prototype.skewY=function(y,c,b){
  this.skew(new Angle(0), y, c, b);
};

Manipulable.prototype.flipH=function(c,b){
  this.scaleX(-1, c, b);
};

Manipulable.prototype.flipV=function(c,b){
  this.scaleY(-1, c, b);
};

// duplication code. Here be dragons.

Manipulable.prototype.duplicate=function(b,id2){
  if(this.node.id==id2) alert(graaaaaaou[42]);
  console.log("duplicating_manipulable", this.node.id, "into", id2);
  var n=this.node.cloneNode(false);
  if(!id2) n.setAttribute('id', "ijs"+this.type+(++__id__));
  else n.setAttribute('id', id2);
  var i={};
  SvgObject.call(i, n);
  i.__proto__=this.__proto__;
  i.type=this.type;
  i.center=this.center;
  i.matrix=new Matrix(this.matrix.a, this.matrix.b, this.matrix.c,
      this.matrix.d, this.matrix.e, this.matrix.f);
  i.sel();
  if(i.d2s) i.d2s();
  this.parent.add(i);
  if(!b) send({
    type: "copy",
    id: this.node.id,
    objId: i.node.id
  });
  console.log(this.node.id, "=", this.node.instance.node.id);
  console.log(i.node.id, "=", i.node.instance.node.id);
  return i;
};

Manipulable.prototype.duplicate2=function(b,dest){
  var xuuu=Manipulable.prototype.duplicate.call(this, true, null);
  if(this.node.id==xuuu.node.id) alert(graaaaaaou[42]);
  get(dest).add(xuuu);
  if(!b) send({
    type: "copy",
    id: this.node.id,
    objId: xuuu.node.id
  });
  if(!b) send({
    type: "move",
    id: xuuu.node.id,
    objParent: dest,
    objPosition: -1
  });
  return xuuu;
};
