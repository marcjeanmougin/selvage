"use strict";
/*
 * computes bounding boxes with builtin getBoundingClientRect, mainly for
 * showing up selection rectangles (but also for selection)
 */

function Box(element){
  this.x=0;
  this.y=0;
  this.width=0;
  this.height=0;
  if(element){
    var box=element.node.getBoundingClientRect();
    var p=new Point(box.left, box.top).s2c();
    this.x=p.x;
    this.y=p.y;
    var vb=(window.vb) ? window.vb : {
      zoom: 1
    };
    this.width=box.width/vb.zoom;
    this.height=box.height/vb.zoom;
  }
}

Box.prototype.merge=function(box){
  var b=new Box();
  b.x=Math.min(this.x, box.x);
  b.y=Math.min(this.y, box.y);
  b.width=Math.max(this.x+this.width, box.x+box.width)-b.x;
  b.height=Math.max(this.y+this.height, box.y+box.height)-b.y;
  return b;
};

Box.prototype.contains=function(b){
  return (this.x<=b.x)&&(this.y<=b.y)&&(this.x+this.width>=b.x+b.width)
      &&(this.y+this.height>=b.y+b.height);
};
