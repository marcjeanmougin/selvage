"use strict";
/*
 * Self-explanatory.
 * 
 */

function Angle(alpha,radian){
  if(!radian){
    this.deg=alpha;
    this.rad=alpha*Math.PI/180;
  }else{
    this.rad=alpha;
    this.deg=alpha/Math.PI*180;
  }
}

Angle.prototype.cos=function(){
  return Math.cos(this.rad);
};
Angle.prototype.sin=function(){
  return Math.sin(this.rad);
};
Angle.prototype.tan=function(){
  return Math.tan(this.rad);
};

Angle.prototype.add=function(o){
  return new Angle((this.deg+o.deg)%360);
};
Angle.prototype.minus=function(o){
  return new Angle((this.deg-o.deg)%360);
};
