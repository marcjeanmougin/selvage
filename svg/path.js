"use strict";
/*
 * A path is a list of segments, and is mainly defined by its "d" attribute. The
 * 2 functions d2s and s2d are helpful to generate a list of segments from the d
 * attr or the reverse Most functions here are to manipulate the path : add a
 * segment (at the end ... or not)
 * 
 * Only 3 types of segments are really handled : cubic bezier curves, lines, and
 * "move". All others should be correctly imported, but cannot be edited.
 * 
 * this.points is supposed to contain a list of all points at end of segments.
 * movePoint() moves a point. pointAt is supposed to give the coords of a point
 * 
 * TODO : really understand (and fully support) 'z' segments
 * 
 */

function Path(d){
  SvgObject.call(this, create("path"));
  this.type="path";
  d=d||"M0 0";
  // this.node.setAttribute("fill", "none");
  // this.node.setAttribute("stroke", "#000");
  // this.node.setAttribute("stroke-width", 1);
  this.setD(d);
};

Path.prototype=new SvgObject();
Path.prototype.constructor=Path;

Path.prototype.setD=function(d){
  this.node.setAttribute("d", d);
  this.segments=this.d2s(d);
};

Path.prototype.d2s=function(d){// updates "segments" from string d
  d=d||this.node.getAttribute("d");
  
  var del=d.split(/[aAcCqQsStTlLmMvVhHzZ]/).slice(1);
  var az=d.split(/[^aAcCqQsStTlLmMvVhHzZ]+/);// Mll
  var x=[];
  var encours=[];
  az.forEach(function(a,ii){// for each "letter"
    if(a=="") return;
    var t=del[ii].split(/[^0-9\.e-]/);// split corresponding section with commas
    // and spaces
    var lastType=a;
    if(lastType=='z'||lastType=='Z'){
      x.push(new Segment(lastType, []));
    }else t.forEach(function(d,id){// for each number
      do{
        var z="";
        if(lastType!='z'&&lastType!='Z'&&d!=""){// special case
          z=parseFloat(d);
          if(z!=null) encours.push(z);// account for errors
        }
        if(encours.length==segmentTypes[lastType]){
          var ee=[];
          if(lastType.match(/[hH]/)){
            ee.push(new Point(encours[0], 0));
          }else if(lastType.match(/[vV]/)){
            ee.push(new Point(0, encours[0]));
          }else for( var i=0; i<segmentTypes[lastType]; i+=2){
            ee.push(new Point(encours[i], encours[i+1]));
          }
          x.push(new Segment(lastType, ee));
          if(lastType.match(/[mM]/)) lastType="l";
          encours=[];
        }
        d=d.slice((""+z+"").length);
      }while(d!="");
    });
  });
  this.segments=x;
  this.recomputePoints();
  return x;
};

Path.prototype.s2d=function(s,b){// given a segment array, updates d
  s=s||this.segments;
  this.set("d", s.join(''), b);
  this.recomputePoints();
  return this;
};

Path.prototype.lastPoint=function(){
  return this.points[this.segments.length-1];
};

Path.prototype.addSegment=function(s){
  if(s.type!="z"&&s.type!="Z"){
    if(s.isAbsolute()) this.points.push(s.point());
    else this.points.push(this.lastPoint().add(s.point()));
  }else{
    console.log(this, s);
  }
  this.segments.push(s);
  this.set("d", this.node.getAttribute("d")+s);
};

Path.prototype.addSegmentAtPos=function(s,pos){
  var end=this.segments.splice(pos);
  this.segments.push(s);
  this.segments=this.segments.concat(end);
  this.s2d();
  return;
};

Path.prototype.l=function(p){
  this.addSegment(new Segment("l", [p]));
};
Path.prototype.z=function(){
  this.addSegment(new Segment("z", []));
};
Path.prototype.h=function(p){
  this.addSegment(new Segment("h", [p]));
};
Path.prototype.v=function(p){
  this.addSegment(new Segment("v", [p]));
};
Path.prototype.H=function(p){
  this.addSegment(new Segment("H", [p]));
};
Path.prototype.V=function(p){
  this.addSegment(new Segment("V", [p]));
};
Path.prototype.C=function(a,b,c){
  this.addSegment(new Segment("C", [a, b, c]));
};

Path.prototype.getPointCoord=function(n,n2){
  if(n2==null) n2=segmentTypes[this.segments[n].type]/2-1;
  var s=this.segments[n];
  if(s.type=="Z"||s.type=="z") return this.getPointCoord(0);
  if(s.isAbsolute()) return this.segments[n].data[n2];
  else return this.points[n-1].add(this.segments[n].data[n2]);
};

Path.prototype.recomputePoints=function(){
  this.points=[];
  var s=this.segments;
  var lastP=new Point();
  for( var i=0; i<s.length; i++){
    // console.log(i, s[i], s[i].point());
    if(s[i].type=="z"||s[i].type=="Z") this.points[i]=lastP=this.points[0];
    else if(s[i].isAbsolute()){
      this.points[i]=lastP=s[i].point();
    }else{
      this.points[i]=lastP=lastP.add(s[i].point());
    }
  }
};

Path.prototype.movePoint=function(n,v,n2){
  this.segments[n].movePoint(v, n2);
  if(this.segments.length>n+1&&n2==null) this.segments[n+1].movePoint2(v);
  this.s2d();
  return this;
};

Path.prototype.applyMatrix=function(m,b){
  var s=true;
  this.segments.forEach(function(x){
    if((s)&&(x.type=="m")){
      // make sure m0 0 is treated as M 0 0
      x.type='M';
    }
    s=false;
    x.applyMatrix(m);
  });
  this.s2d(null, b);
};

Path.prototype.subdivide=function(s_index,t){
  if(s_index==0) console.log("no subdivision on segment 0 (M)");
  this.d2s();
  var seg=this.segments[s_index];
  switch(seg.type){
  case "z":
  case "Z":
    var v=this.points[0].minus(this.points[this.points.length-1]);
    var s=new Segment("l", [new Point(v.x*t, v.y*t)]);
    this.addSegmentAtPos(s, this.segments.length-1);
    break;
  case "h":
  case "H":
  case "v":
  case "V":
  case "l":
  case "L":
    seg.type="l";
    // simple lineTo case
    var v=this.points[s_index].minus(this.points[s_index-1]);
    var s=new Segment("l", [new Point(v.x*(1-t), v.y*(1-t))]);
    seg.data[0].x=v.x*t;
    seg.data[0].y=v.y*t;
    this.addSegmentAtPos(s, s_index+1);
    break;
  case "c": // I'm so sorry.
    var p0=this.points[s_index-1];
    seg.type="C";
    seg.data[0]=seg.data[0].add(p0);
    seg.data[1]=seg.data[1].add(p0);
    seg.data[2]=seg.data[2].add(p0);
  case "C":
    // implement De Casteljau's algorithm
    var p0=this.points[s_index-1];
    var p1=seg.data[0];
    var p2=seg.data[1];
    var p3=seg.data[2];
    var v1=p1.minus(p0);
    var v2=p2.minus(p1);
    var v3=p3.minus(p2);
    var p4=p0.add(new Point(v1.x*t, v1.y*t));
    var p5=p1.add(new Point(v2.x*t, v2.y*t));
    var p6=p2.add(new Point(v3.x*t, v3.y*t));
    var v4=p5.minus(p4);
    var v5=p6.minus(p5);
    var p7=p4.add(new Point(v4.x*t, v4.y*t));
    var p8=p5.add(new Point(v5.x*t, v5.y*t));
    var v6=p8.minus(p7);
    var p9=p7.add(new Point(v6.x*t, v6.y*t));
    var s=new Segment("C", [p8, p6, p3]);
    seg.data[0]=p4;
    seg.data[1]=p7;
    seg.data[2]=p9;
    this.addSegmentAtPos(s, s_index+1);
    break;
  default:
    return;
  }
};

// pointAt(nth segment, t)

Path.prototype.pointAt=function(n,t){
  if(n==0) this.segments[n].pointAt(t, new Point());
  else this.segments[n].pointAt(t, this.points[n-1]);
};

Path.prototype.getArgs=function(){
  var val=this.node.getAttribute("d");
  console.assert(val!="M"); // TODO make it work
  return [val];
};
