"use strict";
/*
 * Gradients. They contain stops, and that's pretty much everything.
 * 
 * Linear gradients go from a point to a point in the [0,1] x [0,1] square,
 * expanded in the Bounding rectangle.
 * 
 * Stops are placed on a [0,1] segment, and colors are interpolated from them.
 * 
 * Radial gradients have the same behavior ([0,1]x[0,1]), with a center and a
 * focal point
 * 
 */

/***/
/** Stop */
/***/
function Stop(){
  SvgObject.prototype.constructor.call(this, create("stop"));
  this.type="stop";
}
Stop.prototype=new SvgObject();
Stop.prototype.constructor=Stop;

Stop.prototype.setColor=function(c){
  this.set("stop-color", c);
};

Stop.prototype.setOffset=function(o){// percentage
  if(o<0||o>1) return;
  this.set("offset", ""+o);
};

Stop.prototype.setOpacity=function(o){
  if(o<0||o>1) return;
  this.set("stop-opacity", ""+o);
};

/***/
/** Gradient : stops, etc. */
/***/
function Gradient(node){
  Container.prototype.constructor.call(this, node);
}
Gradient.prototype=new Container();
Gradient.prototype.constructor=Gradient;

/** Linear (from, to) */
function LinearGradient(p1,p2){
  Gradient.call(this, create("linearGradient"));
  this.type="linearGradient";
  if(!p1) return this;
  this.node.setAttribute("x1", p1.x);
  this.node.setAttribute("y1", p1.y);
  this.node.setAttribute("x2", p2.x);
  this.node.setAttribute("y2", p2.y);
}
LinearGradient.prototype=new Gradient();
LinearGradient.prototype.constructor=LinearGradient;
LinearGradient.prototype.getArgs=function(){
  var x1=parseFloat(this.node.getAttribute("x1"));
  var y1=parseFloat(this.node.getAttribute("y1"));
  var x2=parseFloat(this.node.getAttribute("x2"));
  var y2=parseFloat(this.node.getAttribute("y2"));
  return [new Point(x1, y1), new Point(x2, y2)];
};

/** Radial (center, radius, focal) */

function RadialGradient(c,r,f){
  Gradient.call(this, create("radialGradient"));
  this.type="radialGradient";
  if(!c) return this;
  if(!f) f=c;
  this.node.setAttribute("cx", c.x);
  this.node.setAttribute("cy", c.y);
  this.node.setAttribute("fx", f.x);
  this.node.setAttribute("fy", f.y);
  this.node.setAttribute("r", r);
}
RadialGradient.prototype=new Gradient();
RadialGradient.prototype.constructor=RadialGradient;
RadialGradient.prototype.getArgs=function(){
  var cx=parseFloat(this.node.getAttribute("cx"));
  var cy=parseFloat(this.node.getAttribute("cy"));
  var fx=parseFloat(this.node.getAttribute("fx"));
  var fy=parseFloat(this.node.getAttribute("fy"));
  var r=parseFloat(this.node.getAttribute("r"));
  return [new Point(cx, cy), r, new Point(fx, fy)];
};
