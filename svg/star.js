"use strict";
/*
 * Star and regular polygon functions
 */

//
function Star(p,c,prefs,b){
  Path.call(this);
  this.type="star";
  var r=p.norm();
  var d=this.draw_star(r, prefs.n_pics, r*prefs.star_ratio, prefs.poly, 0);
  // console.log(d);
  this.d2s(d);
  this.node.setAttribute("fill", "#000"); // TODO why is this done?
  this.rotate(p.angle(), new Point(), true);
  this.translate(c, true);// !\\//!!!
};

Star.prototype=new Path();
Star.prototype.constructor=Star;

Star.prototype.draw_star=function(r_ext,n_pic,r_int,poly,angle_dec){
  angle_dec=angle_dec||0;
  var s="M"+r_ext+" 0L";
  for( var i=0; i<n_pic; i++){
    s+=Math.cos(2*Math.PI*i/n_pic)*r_ext+","+Math.sin(2*Math.PI*i/n_pic)*r_ext
        +" ";
    if(!poly){
      s+=Math.cos(2*Math.PI*(i+0.5)/n_pic+angle_dec)*r_int+","
          +Math.sin(2*Math.PI*(i+0.5)/n_pic+angle_dec)*r_int+" ";
    }
  }
  s+="z";
  return s;
};

Star.prototype.toPath=function(){
  
};

Star.prototype.getArgs=function(){
  // TODO should pass the three first arguments rather than the complete path
  var val=this.node.getAttribute("d");
  console.assert(val!="M"); // TODO make it work
  return [null, null, null, val];
}
