"use strict";
/*
 * http://www.w3.org/TR/SVG/painting.html#MarkerElement
 * 
 * see spec (and understand it) before doing ANYTHING here.
 * 
 */
//
function Marker(){
  Container.call(this, create("marker"));
  this.type="marker";
};

Marker.prototype=new Container();
Marker.prototype.constructor=Marker;
