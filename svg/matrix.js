"use strict";
/*
 * 
 * Matrix.js
 * 
 * Transformation matrix ( a c e ) ( b d f ) ( 0 0 1 )
 * 
 */

function Matrix(a,b,c,d,e,f){
  if(a==null) a=1;
  if(b==null) b=0;
  if(c==null) c=0;
  if(d==null) d=1;
  if(e==null) e=0;
  if(f==null) f=0;
  this.a=a;
  this.b=b;
  this.c=c;
  this.d=d;
  this.e=e;
  this.f=f;
}

Matrix.prototype.multiply=function(m2){
  "use asm";
  if(m2==null) m2=new Matrix();
  return new Matrix(this.a*m2.a+this.c*m2.b, this.b*m2.a+this.d*m2.b, this.a
      *m2.c+this.c*m2.d, this.b*m2.c+this.d*m2.d, this.a*m2.e+this.c*m2.f
      +this.e, this.b*m2.e+this.d*m2.f+this.f);
};

Matrix.prototype.apply=function(p){
  "use asm";
  return new Point(this.a*p.x+this.c*p.y+this.e, this.b*p.x+this.d*p.y+this.f);
};

Matrix.prototype.apply_relative=function(p){
  "use asm";
  return new Point(this.a*p.x+this.c*p.y, this.b*p.x+this.d*p.y);
};

Matrix.prototype.invert=function(){
  "use asm";
  var det=this.a*this.d-this.b*this.c;
  if(!det){
    alert("MATRICE NON INVERSIBLE");
    console.log(this);
    return new Matrix();
  }
  return new Matrix(this.d/det, -this.b/det, -this.c/det, this.a/det, (this.c
      *this.f-this.d*this.e)
      /det, (this.e*this.b-this.a*this.f)/det);
};
