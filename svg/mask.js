"use strict";
/*
 * http://www.w3.org/TR/SVG/masking.html#Masking
 * 
 * see spec before doing anything. Please.
 * 
 */
//
function Mask(){
  Container.call(this, create("mask"));
  this.type="mask";
};

Mask.prototype=new Container();
Mask.prototype.constructor=Mask;
