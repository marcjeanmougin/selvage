"use strict";
// style ELEMENT (contains css)
//
function StyleElement(){
  Container.call(this, create("style"));
  this.type="styleElement";
  
  // this.parseContent(content);
};

StyleElement.prototype=new Container();
StyleElement.prototype.constructor=StyleElement;

StyleElement.prototype.parseContent=function(content,uid){
  
  if(!content) content="";
  var doc=document.implementation.createHTMLDocument("");
  var styleElement=document.createElement("style");
  styleElement.textContent=content;
  doc.body.appendChild(styleElement);
  console.log(styleElement, styleElement.sheet.cssRules);
  // iterate on styleElement.sheet.cssRules elements, using .selectorText and
  // .style (to be processed separately)
  
};
