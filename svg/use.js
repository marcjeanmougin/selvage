"use strict";
/*
 * Unlike ‘image’, the ‘use’ element cannot reference entire files.
 * 
 * Clone basic item.
 */

function Use(what){
  SvgObject.call(this, create("use"));
  this.type="use";
  if(what) this.node.setAttributeNS(NS_XLINK, "xlink:href", what);
};

Use.prototype=new SvgObject();
Use.prototype.constructor=Use;
Use.prototype.getArgs=function(){
  return [this.node.getAttributeNS(NS_XLINK, "href")];
};
