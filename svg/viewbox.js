"use strict";
// TODO: check, clean,improve

function ViewBox(element){
  var x, y, width, height, box=element.box(), view=(element.node
      .getAttribute('viewBox')||'').split(' ');
  
  /* clone attributes */
  this.x=box.x;
  this.y=box.y;
  this.width=element.node.offsetWidth||element.node.getAttribute('width');
  this.height=element.node.offsetHeight||element.node.getAttribute('height');
  
  // /personal note : just hope that the height is not a default "100%" or it
  // will bug.
  // personal note from another person : fuck you
  
  if(view.length>3){
    /* get width and height from viewbox */
    x=parseFloat(view[0]);
    y=parseFloat(view[1]);
    width=parseFloat(view[2]);
    height=parseFloat(view[3]);
    
    /* calculate zoom accoring to viewbox */
    this.zoom=((this.width/this.height)>(width/height)) ? this.height/height
        : this.width/width;
    
    /* calculate real pixel dimensions on parent SVG.Doc element */
    this.x=x;
    this.y=y;
    this.width=width;
    this.height=height;
  }
  
  /* ensure a default zoom value */
  this.zoom=this.zoom||1;
  
};
