"use strict";
// ellipse

function Ellipse(p,p2){
  SvgObject.call(this, create("ellipse"));
  this.type="ellipse";
  if(!p) return this;
  var rx=Math.abs(p.x-p2.x)/2;
  var ry=Math.abs(p.y-p2.y)/2;
  this.node.setAttribute("rx", rx);
  this.node.setAttribute("ry", ry);
  this.node.setAttribute("cx", Math.min(p.x, p2.x)+rx);
  this.node.setAttribute("cy", Math.min(p.y, p2.y)+ry);
  
};

Ellipse.prototype=new SvgObject();
Ellipse.prototype.constructor=Ellipse;
Ellipse.prototype.getArgs=function(){
  var rx=parseFloat(this.node.getAttribute("rx"));
  var ry=parseFloat(this.node.getAttribute("ry"));
  var cx=parseFloat(this.node.getAttribute("cx"));
  var cy=parseFloat(this.node.getAttribute("cy"));
  return [new Point(cx-rx, cy-ry), new Point(cx+rx, cy+ry)];
};
