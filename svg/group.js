"use strict";
/*
 * The ‘g’ element is a container element for grouping together related graphics
 * elements.
 */
//
function Group(){
  Container.call(this, create("g"));
  this.type="group";
};

Group.prototype=new Container();
Group.prototype.constructor=Group;

Group.prototype.ungroup=function(){
  if(selection.has(this.node.id)) selection.minus(this.node.id);
  var p=this.parent;
  var x=this.position();
  while(this.children().length){
    var e=this.children()[this.children().length-1];
    p.add(e, x);
    send({
      type: "move",
      id: e.node.id,
      objParent: p.node.id,
      objPosition: x
    });
    
    e.applyMatrix(this.matrix);
    
    // TODO : make a list of selectable and non-selectable things
    if(e.type!="defs") e.sel();
    
    selection.add(e.node.id);
  }
  this.remove();
};
