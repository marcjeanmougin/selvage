"use strict";
// useful various stuff. first file to be included in the html, should not
// depend on anything.

var NS_SVG='http://www.w3.org/2000/svg';
var NS_XLINK='http://www.w3.org/1999/xlink';
var __id__=Math.floor(Math.random()*1000000);

var create=function(name){
  var element=document.createElementNS(NS_SVG, name);
  element.setAttribute('id', "ijs"+name+(++__id__));
  return element;
};

var get=function(id){
  var node=document.getElementById(id);
  if(node) return node.instance;
};

var dashToCamel=function(str){
  return str.replace(/\W+(.)/g, function(x,chr){
    return chr.toUpperCase();
  });
};

var sign=function(x){
  return x ? (x<0 ? -1 : 1) : 0;
};

var signb=function(x){
  return x ? (x<0 ? -1 : 1) : 1;
};

var xor=function(x,y){
  return x ? !y : y;
};

var StylingProperties=["font", "font-family", "font-size", "font-size-adjust",
    "font-stretch", "font-style", "font-variant", "font-weight", "direction",
    "letter-spacing", "text-decoration", "unicode-bidi", "word-spacing",
    "clip", "color", "cursor", "display", "overflow", "visibility",
    "clip-path", "clip-rule", "mask", "opacity", "enable-background", "filter",
    "flood-color", "flood-opacity", "lighting-color", "stop-color",
    "stop-opacity", "pointer-events", "color-interpolation",
    "color-interpolation-filters", "color-profile", "color-rendering", "fill",
    "fill-opacity", "fill-rule", "image-rendering", "marker", "marker-end",
    "marker-mid", "marker-start", "shape-rendering", "stroke",
    "stroke-dasharray", "stroke-dashoffset", "stroke-linecap",
    "stroke-linejoin", "stroke-miterlimit", "stroke-opacity", "stroke-width",
    "text-rendering", "alignment-baseline", "baseline-shift",
    "dominant-baseline", "glyph-orientation-horizontal",
    "glyph-orientation-vertical", "kerning", "text-anchor", "writing-mode"];

String.prototype.replace2=function(a,b){
  return this.split(a).join(b);
};
