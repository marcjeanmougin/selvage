"use strict";
/*
 * not really used. line, polyline, polygon, are mere instances of paths, that
 * imo should not even be in the svg standard (and not very well supported by
 * selvage)
 */
//
function Line(p1,p2){
  SvgObject.call(this, create('line'));
  this.type="line";
  if(!p2) return this;
  this.node.setAttribute("x1", p1.x);
  this.node.setAttribute("y1", p1.y);
  this.node.setAttribute("x2", p2.x);
  this.node.setAttribute("y2", p2.y);
};
Line.prototype=new SvgObject();
Line.prototype.constructor=Line;
Line.prototype.getArgs=function(){
  return [
      new Point(parseFloat(this.node.getAttribute("x1")), parseFloat(this.node
          .getAttribute("y1"))),
      new Point(parseFloat(this.node.getAttribute("x2")), parseFloat(this.node
          .getAttribute("y2")))];
};

function Polyline(d){
  SvgObject.call(this, create('polyline'));
  this.type="polyline";
  if(!d) return this;
  this.node.setAttribute("points", d);
};
Polyline.prototype=new SvgObject();
Polyline.prototype.constructor=Polyline;
Polyline.prototype.getArgs=function(){
  return [this.node.getAttribute("points")];
};

function Polygon(d){
  SvgObject.call(this, create('polygon'));
  this.type="polygon";
  if(!d) return this;
  this.node.setAttribute("points", d);
};
Polygon.prototype=new Polyline();
Polygon.prototype.constructor=Polygon;
