"use strict";
// TODO: document this code

/*
 * Hi. A segment is defined according to the SVG Spec (see it for all details).
 * 
 * segmentTypes lists all possible types, with the following notation :
 * 
 * z (=Z) : closepath : go to first point h (x) : horizontal v (x) : vertical m
 * (p) : "move with the pen lifted" (used) l (p) : line (used) t (?) : no idea.
 * see spec. s (?) : no idea. see spec. q (p p) : quadractic bezier curve c (p p
 * p) : cubic bezier curve (used) a (stuff) : arc (see spec)
 * 
 * uppercase equivalents are defined in absolute coordinates
 * 
 * "data" is(/should be) a list of points
 * 
 */

var segmentTypes={// nb of coords
  "z": 0,
  "Z": 0,
  "h": 1,
  "H": 1,
  "v": 1,
  "V": 1,
  "m": 2,
  "M": 2,
  "l": 2,
  "L": 2,
  "t": 2,
  "T": 2,
  "s": 4,
  "S": 4,
  "q": 4,
  "Q": 4,
  "c": 6,
  "C": 6,
  "a": 7,
  "A": 7
};

// source : implicit because of relative segments
function Segment(type,data){
  this.type=type;
  this.data=data; // supplied as an array of segmentTypes[type] Points
}
Segment.prototype.isAbsolute=function(){
  return (this.type.match(/[A-Z]/)); // valeur de retour chelou, mais marche
};

// useful for exporting the "d" argument
Segment.prototype.toString=function(){
  var a=this.type;
  if(a=='v'||a=='V') return a+this.data[0].y;
  if(a=='h'||a=='H') return a+this.data[0].x;
  this.data.forEach(function(elt,i){
    if(a.match(/^[aA]/)&&i==3){
      a+=elt.x+" ";
    }else a+=(elt+" ");
  });
  return a;
};

Segment.prototype.point=function(){
  if(this.type=='z'||this.type=='Z') return new Point();
  return this.data[Math.floor((segmentTypes[this.type]+1)/2)-1];
};

// moving a point of this segment
Segment.prototype.movePoint=function(v,n2){
  if(n2==null) n2=this.data.length-1;
  
  switch(this.type){
  case "l":
  case "L":
  case "M":
  case "m":
    this.data[0]=this.data[0].add(v);
    break;
  case "c":
  case "C":
    if(n2==this.data.length-1){
      this.data[1]=this.data[1].add(v);
      this.data[2]=this.data[2].add(v);
    }else{
      this.data[n2]=this.data[n2].add(v);
    }
    break;
  }
  
};

// "previous" point has been moved, but we don't really want the whole rest of
// the curve moved
Segment.prototype.movePoint2=function(v){
  switch(this.type){
  case "l":
    this.data[0]=this.data[0].minus(v);
    break;
  case "c":
    this.data[1]=this.data[1].minus(v);
    this.data[2]=this.data[2].minus(v);
    break;
  case "C":
    this.data[0]=this.data[0].add(v);
    break;
  }
};

// getting around some bug : applyMatrix applies to coords
Segment.prototype.applyMatrix=function(m){
  if(this.isAbsolute()){
    for( var i=0; i<this.data.length; i++){
      this.data[i]=this.data[i].applyMatrix(m);
    }
  }else{
    for( var i=0; i<this.data.length; i++){
      this.data[i]=this.data[i].applyPartialMatrix(m);
    }
  }
};

// getting the coords of point at t.
// starting point is provided by caller
Segment.prototype.pointAt=function(t,startpt){
  switch(this.type){
  case "m":
  case "M":
    return startpt;
  case "l":
    return startpt.add(new Point(this.points[0].x*t, this.points[0].y*t));
  case "L":
    var p=this.points[0].minus(startpt);
    return startpt.add(new Point(p.x*t, p.y*t));
  case "c":
    var p0=startpt;
    var p1=this.points[0].add(startpt);
    var p2=this.points[1].add(startpt);
    var p3=this.points[2].add(startpt);
    return new Point(p0.x*(1-t)*(1-t)*(1-t)+p1.x*t*(1-t)*(1-t)+p2.x*t*t*(1-t)
        +p3.x*t*t*t, p0.y*(1-t)*(1-t)*(1-t)+p1.y*t*(1-t)*(1-t)+p2.y*t*t*(1-t)
        +p3.y*t*t*t);
  case "C":
    var p0=startpt;
    var p1=this.points[0];
    var p2=this.points[1];
    var p3=this.points[2];
    return new Point(p0.x*(1-t)*(1-t)*(1-t)+p1.x*t*(1-t)*(1-t)+p2.x*t*t*(1-t)
        +p3.x*t*t*t, p0.y*(1-t)*(1-t)*(1-t)+p1.y*t*(1-t)*(1-t)+p2.y*t*t*(1-t)
        +p3.y*t*t*t);
  default:
    console.log("TODO segment.js / Segment.prototype.pointAt /"+this);
    return new Point();
  }
};
