"use strict";
/*
 * TODO : find a good structure for natural text handling
 * 
 * 
 * 
 */

function Text(){
  SvgObject.call(this, create("text"));
  this.type="text";
  this._style={};
  this.content="";
};

Text.prototype=new Container();
Text.prototype.constructor=Text;

Text.prototype.clear=function(){
  for( var i=this.children().length-1; i>=0; i--)
    this.children()[i].remove();
  return this;
};

function TSpan(){
  SvgObject.call(this, create('tspan'));
  this.type="tspan";
}

TSpan.prototype=new Text();
TSpan.prototype.constructor=TSpan;

function TextNode(text){
  return document.createTextNode(text||"");
};

TextNode.prototype.getArgs=function(){
  return [this.data];
};
