"use strict";
/*
 * "Container" is a parent class for all classes that can contain something :
 * Texts that can contain tspans, gradients that contain stops, group, defs,
 * etc.
 * 
 * We do NOT check that the inclusion is valid (e.g. no error thrown if a text
 * contains a circle)
 * 
 */

function Container(node){
  SvgObject.call(this, node);
  this.type="container";// should not happen
  
};
// inheritance
Container.prototype=new SvgObject();
Container.prototype.constructor=Container;

// svg.js code

// Returns all child elements
Container.prototype.children=function(){
  return this._children||(this._children=[]);
};

// Add given element at a position
Container.prototype.add=function(element,i){
  if(!this.has(element)){
    /* define insertion index if none given */
    i=i==null ? this.children().length : i;
    
    /* remove references from previous parent */
    if(element.parent){
      var index=element.parent.children().indexOf(element);
      element.parent.children().splice(index, 1);
    }
    
    /* add element references */
    this.children().splice(i, 0, element);
    // console.log("trying to add", element.node, "to", this.node, "par", i); //
    // way too verbose
    if(element.node.id==this.node.id) alert(xkeughf[222]);
    this.node.insertBefore(element.node, this.node.childNodes[i]||null);
    element.parent=this;
  }else{
    this.removeElement(element);
    this.add(element, i);
  }
  
  return this;
};

// Basically does the same as `add()` but returns the added element instead
Container.prototype.put=function(element,i){
  this.add(element, i);
  return element;
};

// Checks if the given element is a child
Container.prototype.has=function(element){
  return this.children().indexOf(element)>=0;
};

// Iterates over all children and invokes a given block
Container.prototype.each=function(block){
  var index, children=this.children();
  
  for(index=0, length=children.length; index<length; index++)
    if(children[index]instanceof(SvgObject)) block.apply(children[index], [
        index, children]);
  
  return this;
};

// Remove a child element at a position
Container.prototype.removeElement=function(element){
  var i;
  if(this.node&&element.node&&(i=this.children().indexOf(element))!=-1){
    // console.log(this.node, element.node);
    this.node.removeChild(element.node);
    this.children().splice(i, 1);
    element.parent=null;
  }
  return this;
};

Container.prototype.create=function(o,silent){
  // Do not create an image when the source is empty
  if(o.type){
    if(o.type=="image"&&!o.getArgs()[0]) return;
    // Only one defs element
    if(o.type=="defs"){
      var fff=0;
      this.each(function(e){
        if(this.type=="defs") fff=this;
      });
      if(fff) return fff;
    }
  }
  var a=null;
  var args=null;
  var type=null;
  var id="-1";
  if(!o.type){
    // textNodes are not proper objects
    a=this.node.appendChild(o);
    type="textNode";
    args=[o.data];
  }else{
    a=this.put(o);
    id=a.node.id;
    args=o.getArgs();
    type=o.type;
    if(o.type=="image"){
      a.set("width", a.node.attributes['width'].value);
      a.set("height", a.node.attributes['height'].value);
      a.set("x", a.node.attributes['x'].value);
      a.set("y", a.node.attributes['y'].value);
    }
  }
  if(!silent) send({
    type: "new",
    objId: id,
    objParent: this.node.id,
    objType: type,
    objArgs: args,
  });
  return a;
};

Container.prototype.uiArrow=function(dir,m){
  var a=this.put(new Arrow(dir, m));
  return a;
};

Container.prototype.uiHandle=function(p,n,n2){
  var a=this.put(new Handle(p, n, n2));
  return a;
};

// Get the viewBox and calculate the zoom value
Container.prototype.viewbox=function(v){
  if(arguments.length==0)
  /* act as a getter if there are no arguments */
  return new ViewBox(this);
  
  /* otherwise act as a setter */
  v=arguments.length==1 ? [v.x, v.y, v.width, v.height] : [].slice
      .call(arguments);
  
  return this.node.setAttribute('viewBox', v.join(' '));
};

// Remove all elements in this container
Container.prototype.clear=function(){
  /* remove children */
  for( var i=this.children().length-1; i>=0; i--)
    this.removeElement(this.children()[i]);
  
  /* remove defs node */
  if(this._defs){
    this._defs.remove();
    delete this._defs;
  }
  
  return this;
};

Container.prototype.duplicate=function(b){// direct call from UI
  return this.duplicate2(b, this.parent.node.id);
};

Container.prototype.duplicate2=function(b,dest){
  var x=Manipulable.prototype.duplicate2.call(this, b, dest);
  var t=this.children().slice();
  for( var i=0; i<t.length; i++){
    var a=t[i].duplicate2(b, x.node.id);
    a.unsel();
  }
  return x;
};

var SVG=function(element){
  console.log("SVG constructor", element);
  /* ensure the presence of a html element */
  this.parent=typeof element=='string' ? document.getElementById(element)
      : element;
  
  /*
   * If the target is an svg element, use that element as the main wrapper. This
   * allows svg.js to work with svg documents as well.
   */
  this.constructor.call(this, this.parent.nodeName=='svg' ? this.parent
      : create('svg'));
  
  /* set svg element attributes and create the <defs> node */
  this.node.setAttribute("xmlns", NS_SVG);
  this.node.setAttribute("xmlns:xlink", NS_XLINK);
  this.node.setAttribute("id", "drawingSvg");
  this.node.setAttribute("version", "1.1");
  this.node.setAttribute("width", "100%");
  this.node.setAttribute("height", "100%");
  this.parent.appendChild(this.node);
  
};

// Inherits from SVG.Container
SVG.prototype=new Container;

// create object given type and args
function createObject(obj,objArgs){
  switch(obj){
  case "rect":
    return new Rect(objArgs[0], objArgs[1]);
  case "star":
    // TODO: use the first arguments to recreate the star
    return new Path(objArgs[3]);
  case "ellipse":
  case "circle":
    return new Ellipse(objArgs[0], objArgs[1]);
  case "path":
    return new Path(objArgs[0]);
  case "g":
  case "group":
    return new Group();
  case "clipPath":
    return new ClipPath();
  case "mask":
    return new Mask();
  case "symbol":
    return new Symbol();
  case "marker":
    return new Marker();
  case "use":
    return new Use(objArgs[0]);
  case "image":
    return new Image(objArgs[0], objArgs[1], objArgs[2]);
  case "line":
    return new Line(objArgs[0], objArgs[1]);
  case "polyline":
    return new Polyline(objArgs[0]);
  case "polygon":
    return new Polygon(objArgs[0]);
  case "svg":
    return new Nested();
  case "text":
    return new Text();
  case "tspan":
    return new TSpan();
  case "linearGradient":
    return new LinearGradient(objArgs[0], objArgs[1]);
    return;
  case "radialGradient":
    return new RadialGradient(objArgs[0], objArgs[1]);
  case "stop":
    return new Stop();
  case "defs":
    return new Defs();
  case "textNode":
    return new TextNode(objArgs[0]);
  case "style":
  case "styleElement":
    return new StyleElement();
  default:
    console.log("unknown element type: ", obj, objArgs);
  }
};
