"use strict";
/*
 * UI selection artifacts
 */

function UI(){
  this.tempBox=0;
}

UI.prototype.clearSelection=function(){
  selection.selection.forEach(function(x){
    console.log("ui"+x);
    get("ui_"+x).remove(true);
  });
};

UI.prototype.selectObj=function(id){
  var b=get(id).box();
  var x=ui_artifacts.create(new Rect(new Point(b.x, b.y), new Point(
      b.x+b.width, b.y+b.height)), true);
  x.node.setAttribute("id", "ui_"+id);
  x.node.setAttribute("fill", "none");
  x.node.setAttribute("stroke", "#a00");
  x.node.setAttribute("stroke-width", 1/vb.zoom);
};

UI.prototype.unselectObj=function(id){
  get("ui_"+id).remove(true);
};

UI.prototype.update_with_zoom=function(){
  selection.selection.forEach(function(x){
    get("ui_"+x).node.setAttribute("stroke-width", 1/vb.zoom);
  });
};

UI.prototype.draw_rectangle=function(p1,p2){
  if(this.tempBox) this.tempBox.remove(true);
  this.tempBox=ui_artifacts.create(new Rect(p1, p2), true);
  this.tempBox.node.setAttribute("fill", "none");
  this.tempBox.node.setAttribute("stroke", "#f00");
  this.tempBox.node.setAttribute("stroke-width", 1/vb.zoom);
  return this.tempBox;
};

UI.prototype.draw_point=function(p){// to be deleted manually
  var x=ui_artifacts.create(new Rect(p.minus(new Point(3/vb.zoom, 3/vb.zoom)),
      p.add(new Point(3/vb.zoom, 3/vb.zoom))), true);
  x.node.setAttribute("fill", "none");
  x.node.setAttribute("stroke", "#f00");
  x.node.setAttribute("stroke-width", 1/vb.zoom);
  return x;
};
