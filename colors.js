"use strict";
function Colors(){
};

/*
 * $('#gradientEditor').overlay({ load: true, mask: true }).load();
 * 
 */
Colors.refresh=function(){
  var defs=work.create(new Defs);
  var ll=[];
  var truc="<svg width=\"0\" height=\"0\" xmlns=\""
      +NS_SVG
      +"\" version=\"1.1\"><defs><pattern id=\"ui_transparent\" patternUnits=\"userSpaceOnUse\" width=\"10\" height=\"10\">"
      +"<rect x=\"0\" y=\"0\" width=\"5\" height=\"5\" fill=\"black\" />"
      +"<rect x=\"5\" y=\"5\" width=\"5\" height=\"5\" fill=\"black\" />"
      +"<rect x=\"0\" y=\"5\" width=\"5\" height=\"5\" fill=\"white\" />"
      +"<rect x=\"5\" y=\"0\" width=\"5\" height=\"5\" fill=\"white\" />"
      +"</pattern></defs></svg>";
  defs.each(function(){
    if((this.type=="linearGradient"||this.type=="radialGradient")
        &&this.node.firstChild!=undefined){
      ll.push(this.node.id);
    }
  });
  ll
      .forEach(function(a){
        truc+="<div onclick=\"Colors.editGradient('"// give a class
            +a
            +"')\"><svg width=\"90%\" height=\"40\" xmlns=\""
            +NS_SVG
            +"\" version=\"1.1\"><rect width=\"100%\" height=\"50%\" fill=\"white\" /><rect width=\"100%\" height=\"50%\" fill=\"url(#ui_transparent)\" y=\"50%\" /><rect width=\"100%\" height=\"100%\" fill=\"url(#"
            +a
            +")\" stroke=\"blue\" stroke-width=\"2\"/><text font-size='8' y=\"20\" fill=\"black\" stroke=\"#000066\" stroke-width=\"0.5\">"
            +a+"</text></svg></div>";
      });
  $("#gradientEditor_gradientList").html(truc);
  $("#gradientEditor_gradientDetails").html("");
  $("#gradientEditor_gradientPreview").html("Please select a gradient");
};

Colors.begindrag=function(e){
  console.log(e);
};
Colors.enddrag=function(e){
  console.log(e);
};

Colors.editGradient=function(id){
  // $("#gradientEditor_gradientPreview")
  // .html(
  var tmp_preview="<svg width=\"100%\" height=\"100%\" xmlns=\""
      +NS_SVG
      +"\" version=\"1.1\">"
      +"<rect width=\"100%\" height=\"50%\" fill=\"white\" /><rect width=\"100%\" height=\"50%\" fill=\"url(#ui_transparent)\" y=\"50%\" />"
      +"<rect width=\"100%\" height=\"100%\" fill=\"url(#"+id
      +")\" stroke=\"white\" stroke-width=\"8\"/>";
  var mdetails="<table><th><td>offset</td><td>color</td><td>opacity</td></th>";
  var g=get(id).children();
  // console.log(g);
  g
      .forEach(function(stop,i){
        var o=stop.get("offset");
        tmp_preview+="<rect width=\"5\" height=\"5\" x=\""
            +(100*o)
            +"%\" y=\"50%\" fill=\"blue\" stroke=\"black\" stroke-width=\"1\""
            +"onmousedown(event)=\"Colors.begindrag(event)\" onmouseup=\"Colors.enddrag(event)\" />";
        var a="<tr><td>"+o+"</td><td>";
        a+=stop.get("stop-color")+"</td><td>";
        a+=(stop.get("stop-opacity")=="") ? "1" : stop.get("stop-opacity");
        a+="</td><td>"+(i==0||i==g.length-1) ? "" : "X"+"</td></tr>";
        mdetails+=a;
      });
  tmp_preview+="</svg>";
  $("#gradientEditor_gradientPreview").html(tmp_preview);
  $("#gradientEditor_gradientDetails").html(mdetails+"</table>");
};

Colors.refreshList=function(){
  this.base=work.create(new Defs);
  var ldd=$("#linearGradientDropDown");
  var rdd=$("#radialGradientDropDown");
  var ll="";
  var rl="";
  this.base
      .each(function(){
        if(this.type=="linearGradient"){
          ll+="<li onclick=\"Colors.editLGradient('"
              +this.node.id
              +"')\"><svg width=\"90\" height=\"40\" xmlns=\""
              +NS_SVG
              +"\" version=\"1.1\">"
              +"<rect width=\"100%\" height=\"100%\" fill=\"url(#"
              +this.node.id
              +")\" stroke=\"blue\" stroke-width=\"2\"/>"
              +"<text font-size='8' y=\"20\" fill=\"black\" stroke=\"white\" stroke-width=\"0.5\">"// POC(todo).
              +this.node.id+"</text></svg></li>";
        }
        if(this.type=="radialGradient"){
          rl+="<li onclick=\"Colors.editRGradient('"
              +this.node.id
              +"')\"><svg width=\"90\" height=\"40\" xmlns=\""
              +NS_SVG
              +"\" version=\"1.1\">"
              +"<rect width=\"100%\" height=\"100%\" fill=\"url(#"
              +this.node.id
              +")\" stroke=\"blue\" stroke-width=\"2\"/>"
              +"<text font-size='8' y=\"20\" fill=\"black\" stroke=\"white\" stroke-width=\"0.5\">"// POC(todo).
              +this.node.id+"</text></svg></li>";
        }
      });
  ldd.html(ll);
  rdd.html(rl);
};

Colors.addLinearGradient=function(p1,p2){
  p1=p1||new Point(0, 0);
  p2=p2||new Point(1, 0);
  console.log(p1);
  this.base=work.create(new Defs);
  var a=this.base.create(new LinearGradient(p1, p2));
  var b=a.create(new Stop);
  b.setOffset(0);
  b.setColor("black");
  b=a.create(new Stop);
  b.setOffset(1);
  b.setColor("red");
  this.refreshList();
  this.editLGradient(a.node.id);
};

Colors.editLGradient=function(id){
  
  $("#tab_linearGradient .dropdown dt a")
      .html(
          "<svg width=\"90\" height=\"40\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">"
              +"<rect width=\"100%\" height=\"100%\" fill=\"url(#"
              +id
              +")\" stroke=\"blue\" stroke-width=\"2\"/>"
              +"<text y=\"20\">"
              +id
              +"</text></svg>").parent().parent().find('dd ul').hide();
  
  selection.fill('url(#'+id+')');
  console.log(id);
  var e=get(id);
  var h='(<input style="width:20px;" onchange="get(\''+id
      +'\').set(\'x1\',this.value)" value="'+e.node.getAttribute('x1')
      +'"></input>'+'<input style="width:20px;" onchange="get(\''+id
      +'\').set(\'y1\',this.value)" value="'+e.node.getAttribute('y1')
      +'"></input>)'+'to (<input style="width:20px;" onchange="get(\''+id
      +'\').set(\'x2\',this.value)" value="'+e.node.getAttribute('x2')
      +'"></input>'+'<input style="width:20px;" onchange="get(\''+id
      +'\').set(\'y2\',this.value)" value="'+e.node.getAttribute('y2')
      +'"></input>)'+'<select onChange="Colors.editLStop(this.value)">';
  get(id).each(function(){
    h+='<option>'+this.node.id+'</option>';
  });
  h+='</select><button onclick="get(\''+id
      +'\').create(new Stop)">+</button><button>-</button>'
      +'<button onclick="selection.fill(\'url(#'+id+')\')">Set</button>';
  $("#linearGradientEdit").html(h);
  this.editLStop(e.children()[0].node.id);
};

Colors.editRGradient=function(id){
  $("#tab_radialGradient .dropdown dt a")
      .html(
          "<svg width=\"90\" height=\"40\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">"
              +"<rect width=\"100%\" height=\"100%\" fill=\"url(#"
              +id
              +")\" stroke=\"blue\" stroke-width=\"2\"/>"
              +"<text y=\"20\">"
              +id
              +"</text></svg>").parent().parent().find('dd ul').hide();
  
  selection.fill('url(#'+id+')');
  var e=get(id);
  var h='c=(<input style="width:20px;" onchange="get(\''+id
      +'\').set(\'cx\',this.value)" value="'+e.node.getAttribute('cx')
      +'"></input>'+'<input  style="width:20px;" onchange="get(\''+id
      +'\').set(\'cy\',this.value)"  value="'+e.node.getAttribute('cy')
      +'"></input>)<br/>'+'r=<input  style="width:20px;" onchange="get(\''+id
      +'\').set(\'r\',this.value)"  value="'+e.node.getAttribute('r')
      +'"></input><br/>'+'f=(<input  style="width:20px;" onchange="get(\''+id
      +'\').set(\'fx\',this.value)"  value="'+e.node.getAttribute('fx')
      +'"></input>'+'<input  style="width:20px;" onchange="get(\''+id
      +'\').set(\'fy\',this.value)"  value="'+e.node.getAttribute('fy')
      +'"></input>)'+'<select  onChange="Colors.editRStop(this.value)">';
  
  get(id).each(function(){
    h+='<option>'+this.node.id+'</option>';
  });
  h+='</select><button  onclick="get(\''+id
      +'\').create(new Stop)">+</button><button>-</button>'
      +'<button  onclick="console.log(id);selection.fill(\'url(#'+id
      +')\');">Set</button>';
  $("#radialGradientEdit").html(h);
  this.editRStop(e.children()[0].node.id);
};

Colors.editLStop=function(id){
  var e=get(id);
  var h="color=<input style='width:20px;' value='"+e.get('stop-color')
      +"' onchange='get(\""+id+"\").set(\"stop-color\",this.value)'/><br/>"
      +"offset=<input style='width:20px;' value='"+e.get('offset')
      +"' onchange='get(\""+id+"\").set(\"offset\",this.value)'/><br/>"
      +"opacity=<input style='width:20px;' value='"+e.get('stop-opacity')
      +"' onchange='get(\""+id+"\").set(\"stop-opacity\",this.value)'/>";
  $("#linearGradientStopEdit").html(h);
};

Colors.editRStop=function(id){
  var e=get(id);
  var h="color=<input  style='width:20px;' value='"+e.get('stop-color')
      +"'  onchange='get(\""+id+"\").set(\"stop-color\",this.value)'/><br/>"
      +"offset=<input  style='width:20px;' value='"+e.get('offset')
      +"'  onchange='get(\""+id+"\").set(\"offset\",this.value)'/><br/>"
      +"opacity=<input  style='width:20px;' value='"+e.get('stop-opacity')
      +"'  onchange='get(\""+id+"\").set(\"stop-opacity\",this.value)'/>";
  $("#radialGradientStopEdit").html(h);
};

Colors.addRadialGradient=function(c,r,f){
  c=c||new Point(0.5, 0.5);
  r=r||0.5;
  this.base=work.create(new Defs);
  var a=this.base.create(new RadialGradient(c, r, f));
  var b=a.create(new Stop);
  b.setOffset(0);
  b.setColor("black");
  b=a.create(new Stop);
  b.setOffset(1);
  b.setColor("red");
  this.refreshList();
  this.editRGradient(a.node.id);
};
var colors;
$(function(){
  colors=new Colors();
  
  $("#colorPicker li").each(function(){
    var prefix="c_";
    var color=this.id.substring(prefix.length);
    this.style.backgroundColor=color;
  });
  $("#colorPicker li").mousedown(function(e){
    var color=this.style.backgroundColor;
    if(!e.shiftKey){
      $("#fillColor").val(color);
      $("#fillColor").trigger("change");
    }else{
      $("#strokeColor").val(color);
      $("#strokeColor").trigger("change");
    }
  });
});
